var express          = require('express');
var path             = require('path');
var favicon          = require('serve-favicon');
var logger           = require('morgan');
var cookieParser     = require('cookie-parser')();
var bodyParser       = require('body-parser');
var debug            = require('debug')('yoke:server');
var http             = require('http');
var cors             = require('cors');
var multer           = require('multer');
var cloudinary       = require('cloudinary');
var cloudinaryConfig = require('./server/config/cloudinary_config');
var port             = normalizePort(process.env.PORT || '3000');

var app           = express();
var server        = http.createServer(app);

var io            = require('socket.io')(server);
var socketsRoute  = require('./server/sockets')(io)

var apiRoutes     = require('./server/routes/routes');


// Global cloudinary configuration
	cloudinary.config(cloudinaryConfig)

// APP VARIABLE/GLOBALS
	// view engine setup
	app.set('views', path.join(__dirname, 'server/views'));
	app.set('view engine', 'jade');

	// app.set('models', require('./server/models'));
	app.set('port', port);

// SERVER STEUP
	// uncomment after placing your favicon in /public
	//app.use(favicon(__dirname + '/public/favicon.ico'));
	app.use(cors())
	app.use(logger('dev'));
	app.use(bodyParser.json({limit: '10mb'}));
	app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
	app.use(multer({ dest: './server/uploads/'}))
	app.use(express.static(path.join(__dirname, 'client/public')));

// ROUTE SETUP
	app.use('/api/v1', apiRoutes);

	// Angular JS handling routing on the front end, so any requests that don't already match
	// defined routes above - forward to front end index.html to deal with
	app.get('*', function(req, res){
		res.sendFile('index.html', { root: __dirname + '/client/public' });
	});


// ERRORS
	// catch 404 and forward to error handler
	app.use(forwardErrorToHandlers)

	if (app.get('env') === 'development')
		app.use(showErrorPageDev) // development error handler: will print stacktrace
	else
		app.use(showErrorPageProduction) // production error handler: no stacktraces leaked to user


server.listen(app.get('port'));
server.on('error', onError);
server.on('listening', onListening);


module.exports = app





// error handler functions
function forwardErrorToHandlers(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
}

function showErrorPageDev(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: err
	})
}

function showErrorPageProduction(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	})
}

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
	var port = parseInt(val, 10)

	// named pipe
	if (isNaN(port)) return val

	// port number
	if (port >= 0) return port

	return false
}


/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	var bind = typeof port === 'string'
		? 'Pipe ' + port
		: 'Port ' + port;

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}


/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
	var addr = server.address();
	console.log(addr)
	var bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr.port;
	debug('Listening on ' + bind);
	console.log('listening on port ' + addr.port)
}
