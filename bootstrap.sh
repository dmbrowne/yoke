#!/usr/bin/env bash

apt-get update
apt-get install -y language-pack-en
apt-get install -y build-essential g++
apt-get install -y zsh
apt-get install -y git
apt-get install -y postgresql postgresql-contrib


yokeuser="yoke"
userexists="$(grep ^${yokeuser}: /etc/passwd)"
if [ -n "${userexists}" ]; then
	echo "User ${yokeuser} is on the system"
else
	echo "Adding ${yokeuser} to system"
	adduser --disabled-password --gecos "yoke" yoke
	# usermod -a -G sudo yoke
fi

## Add postgres user yoke if not already created
testuserrole="SELECT 1 FROM pg_roles WHERE rolname='${yokeuser}'"
dbusercmd="psql postgres -tAc '${testuserrole}';"
testdbuserexist="$(sudo -u postgres bash -c '${dbusercmd}')"
if [ testdbuserexist ];
	then
	echo "Postgres user yoke already exists"
else
	sudo -u postgres bash -c "psql -c \"CREATE USER yoke WITH SUPERUSER PASSWORD 'yoke';\""
fi

dbexistcmd="psql -lqt | cut -d \| -f 1 | grep -w '${yokeuser}';"
testdbexist="$(sudo -u postgres bash -c '${dbexistcmd}')"
if [ testdbexist ]; then
    echo "yoke database already exists"
else
	sudo -u postgres bash -c "psql -c \"CREATE DATABASE yoke WITH OWNER yoke;\""
fi


zshprofile="/home/vagrant/.zshrc"
if [ -f "${zshprofile}" ]; then
	echo ".zshrc aready exists."
else
	git clone git://github.com/robbyrussell/oh-my-zsh.git /home/vagrant/.oh-my-zsh
	cp /home/vagrant/.oh-my-zsh/templates/zshrc.zsh-template /home/vagrant/.zshrc
	chsh -s /bin/zsh vagrant
fi

nvmdirectory="/home/vagrant/.nvm"
if [ ! -d "${nvmdirectory}" ]; then
	git clone https://github.com/creationix/nvm.git /home/vagrant/.nvm && cd /home/vagrant/.nvm && git checkout `git describe --abbrev=0 --tags`
	chown -R vagrant:vagrant /home/vagrant/.nvm

	grep -Fxq ". ~/.nvm/nvm.sh" /home/vagrant/.bashrc || echo ". ~/.nvm/nvm.sh" >> /home/vagrant/.bashrc
	grep -Fxq ". ~/.nvm/nvm.sh" /home/vagrant/.zshrc || echo ". ~/.nvm/nvm.sh" >> /home/vagrant/.zshrc

	# nvm
	su vagrant
	nvm install 0.12 && nvm alias default 0.12
	npm install -g sequelize-cli nodemon gulp bower
fi
