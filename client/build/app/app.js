import { default as accountConfirmRoute }   from   './routes/accountConfirm/verify-account.route'
import { default as userDashRoute       }   from   './routes/dash/dash.route'
import { default as chatRoute           }   from   './routes/chat/chat.route'
import { default as loginRoute          }   from   './routes/login/login.route'
import { default as signupRoute         }   from   './routes/signup/signup.route'
import { default as homeRoute           }   from   './routes/home/home.route'
import { default as noteRoute           }   from   './routes/notes/note.route'
import { default as requestInterceptor  }   from   'app/services/requestInterceptor'
import { default as yokeCropper         }   from   'app/components/ykCropper'
import angular                              from   'angular'
import 'angular-ui-router';
import 'angular-resource';
import 'appTemplates';
import 'angular-locker';

var yokeApp = angular.module('yoke', [
	noteRoute.name,
	accountConfirmRoute.name,
	signupRoute.name,
	homeRoute.name,
	loginRoute.name,
	chatRoute.name,
	userDashRoute.name,
	requestInterceptor.name,
	yokeCropper.name,
	'appTemplates',
	'ui.router',
	'ngResource',
	'angular-locker',
	// 'angularMoment',
	// 'infinite-scroll',
	// 'ngAnimate',
	// 'ngSanitize'

])
.config(appConfig)
.run(run);

export default yokeApp;

appConfig.$inject = ['$urlRouterProvider', '$locationProvider', '$httpProvider', 'lockerProvider'];
run.$inject = ['$rootScope', '$state'];

function appConfig($urlRouterProvider, $locationProvider, $httpProvider, lockerProvider){
	$httpProvider.interceptors.push('TokenRequestInterceptor');
	$urlRouterProvider.otherwise("/");
	$locationProvider.html5Mode(true);

	lockerProvider.defaults({
        driver: 'session',
        namespace: 'yke'
    });
}

function run($rootScope, $state){
	$rootScope.$state = $state;
	// amMoment.changeLocale('en');
}
