import 'angular';
import 'angular-mocks';
import {default as appModule} from 'app/app';

describe('app module name', () => {
	it('should equal yoke', () => {
		expect(appModule.name).toEqual('hotline')
	})
})
