import angular from 'angular';
import {default as appMainModule} from './app';

angular.element(document).ready(() => {
	$.material.init();

	angular.bootstrap(document.querySelector('[data-main-app]'), [
		appMainModule.name
	], {
		strictDi: true
	});
});
