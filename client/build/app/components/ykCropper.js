import 'angular'

export default angular.module('yokeCropperModule', [])
	.directive('ykCropper', yokeJqueryCropper)

// yokeJqueryCropper.$inject = []
function yokeJqueryCropper(){
	return {
		restrict: 'A',
		scope: {
			options: '=',
			resultImage: '='
		},
		link: function(scope, element, attrs){
			let cropperReady = false
			let optionDefaults = {
				strict: true,
				guides: true,
			}

			let options = $.extend(optionDefaults, scope.options)

			attrs.$observe('src', newV => {
				if(newV === undefined) return

				element[0].onload = () => {
					$(element).cropper(options)
					cropperReady = true
				}
			})


			$(element).on('dragstart.cropper dragend.cropper', function (e) {
				if(!cropperReady) return

				scope.$apply(()=>{
					scope.resultImage = $(element).cropper('getCroppedCanvas').toDataURL()
				})
			})

		}
	}
}
