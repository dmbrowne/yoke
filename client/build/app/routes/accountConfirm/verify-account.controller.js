import 'angular'

const USERSERVICE = new WeakMap()

class VerifyAccountController{
	constructor(UserService, tokenId, user){
		USERSERVICE.set(this, UserService)
		this.tokenId = tokenId
		this.user = user
	}

	submitVerifyForm(form){
		USERSERVICE.get(this)
			.verify()
			.save({ user_id: this.user.id }, {
				password: form.password.$viewValue,
				verify_token: this.tokenId
			})
			.$promise
			.then( response => { console.log(response) } )
			.catch( reason => { console.log(reason) } )
	}
}

VerifyAccountController.$inject = ['UserService', 'tokenId', 'user']
export default VerifyAccountController
