import 'angular'
import 'angular-ui-router'
import {default as userServiceModule} from 'app/services/api/users'
import {default as verifyAccountContrller} from './verify-account.controller'


var verifyAccountRouteModule = angular
	.module('verifyAccountModule', [
		'ui.router',
		userServiceModule.name
	])
	.config(verifyAccountConfig)

export default verifyAccountRouteModule

verifyAccountConfig.$inject = ['$stateProvider']
function verifyAccountConfig($stateProvider){
	$stateProvider.state('verifyAccount', {
		url:'/verify-account/:user_id?token',
		controller: verifyAccountContrller,
		controllerAs: 'verifyAccCntrlr',
		templateUrl: 'routes/accountConfirm/verify-account.tpl.html',
		resolve: {

			UserService: 'UserService',

			userId: function($stateParams){
				console.log($stateParams.user_id)
				return $stateParams.user_id
			},

			tokenId: function($stateParams){
				console.log($stateParams.token)
				return $stateParams.token
			},

			user: function(UserService, userId){
				console.log('getting user')
				return UserService.users().get({ user_id: userId }).$promise
			}
		}
	})
}
