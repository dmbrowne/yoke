import 'angular';

const controllerName = 'ChatIndexController';
const CURRENTUSERSERVICE = new WeakMap();

class ChatIndexController{
	constructor(CurrentUser, allPartners){
		CURRENTUSERSERVICE.set(this, CurrentUser)
		this.user = CURRENTUSERSERVICE.get(this).user
		this.partners = allPartners
	}
}

ChatIndexController.$inject = ['CurrentUser', 'allPartners'];

export default ChatIndexController;
export {controllerName as name};
