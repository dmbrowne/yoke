import 'angular';
import 'angular-ui-router';
import { default as chatIndexController } from './chat-index.controller';
import { name as chatIndexControllerName } from './chat-index.controller';


var chatIndexModule = angular
	.module('ChatIndex', [])
	.config(chatIndexConfig)
	// .controller(chatIndexControllerName, chatIndexController)


export default chatIndexModule;


chatIndexConfig.$inject = ['$stateProvider'];
function chatIndexConfig($stateProvider){
	$stateProvider.state('chat.index', {
		url: '',
		templateUrl:  'routes/chat/chat-index/chat-index.tpl.html',
		controller:   chatIndexController,
		controllerAs: 'chatIdxCntlr'
	})
}
