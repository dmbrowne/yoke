import 'angular'

class ConversationThreadController{
	constructor(SocketIO, relationshipId, messages, Partner, user){
		this.socket = SocketIO
		this.relationshipId = relationshipId
		this.messages = messages
		this.partner = Partner
		this.user = user
	}

	sendMessage(sendMessageForm){
		if(!sendMessageForm.$valid) return

		this.socket.sendMessage({
			room_id: this.relationshipId,
			message: sendMessageForm.composedMessage.$viewValue
		})
		.then(() => { console.log('sent') })
		.catch(err => { console.log('error', err) })
	}
}

ConversationThreadController.$inject = ['SocketIO', 'relationshipId', 'messages', 'Partner', 'user']

export default ConversationThreadController;
