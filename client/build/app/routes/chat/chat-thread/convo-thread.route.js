import 'angular'
import 'angular-ui-router'
import { default as privateConversationServiceModule } from 'app/services/api/privateConversation';
import { default as conversationThreadContrller } from './convo-thread.controller';


var convoThreadModule = angular
	.module('ConvoThreadModule', [
		'ui.router',
		privateConversationServiceModule.name
	])
	.config(convoThreadConfig)

convoThreadConfig.$inject = ['$stateProvider']

export default convoThreadModule;

function convoThreadConfig($stateProvider){
	'use strict'

	$stateProvider.state('chat.conversationThread', {
		url: '/:relationship_id',
		templateUrl: 'routes/chat/chat-thread/conversation.tpl.html',
		controller: conversationThreadContrller,
		controllerAs: 'conversationThreadCtrlr',
		resolve: {

			_: '_',

			PrivateConversationService: 'PrivateConversationService',

			relationshipId: function($stateParams){
				return $stateParams.relationship_id
			},

			joinroom: function(SocketIO, relationshipId){
				return SocketIO.joinRoom(relationshipId)
					.then( err => {
						if(err) alert('cannot join room');
						return relationshipId
					})
					.catch( err => { alert('cannot join room') ; console.log(err) })
			},

			Partner: function(allPartners, _, relationshipId){
				console.log(_.filter(allPartners, { UserRelationship: { relationship_id: relationshipId } }))
				return _.filter(allPartners, { UserRelationship: { relationship_id: relationshipId } })
			},

			messages: function(PrivateConversationService, relationshipId){
				return PrivateConversationService.conversationByRelationshipId().get({
					id: relationshipId
				})
				.$promise.then( response => {
					return response.PrivateMessages
				})
			}
		}
	})
}
