import 'angular';
import 'angular-ui-router';
import { default as ChatIndexRouteModule } from './chat-index/chat-index.route'
import { default as ConversatioThreadRouteModule } from './chat-thread/convo-thread.route'
import { default as currentUserServiceModule } from 'app/services/currentUser';
import { default as lodashModule } from 'app/services/loDash';
import { default as SocketService } from 'app/services/socket';

var chatRouteModule = angular
	.module('ChatRoute', [
		'ui.router',
		ChatIndexRouteModule.name,
		currentUserServiceModule.name,
		lodashModule.name,
		SocketService.name,
		ConversatioThreadRouteModule.name
	])
	.config(chatRouteConfig)


chatRouteConfig.$inject = ['$stateProvider'];

export default chatRouteModule;

function chatRouteConfig($stateProvider){
	$stateProvider.state('chat', {
		url: '/chat',
		abstract: true,
		template: '<div ui-view />',
		resolve: {

			_: '_',

			CurrentUser: 'CurrentUser',

			SocketIO: 'SocketIO',

			connection: function(SocketIO){
				return SocketIO.connect('/chat')
			},

			user: function(CurrentUser){
				return CurrentUser.user
			},

			allPartners: function(CurrentUser, _){
				return CurrentUser.user.Partner
			},

			activePartner: function(allPartners, _){
				return _.filter(allPartners, { UserRelationship: { deleted_at: null } })
			}
		}
	})
}
