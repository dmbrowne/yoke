import 'angular';
import 'angular-ui-router';
import { default as dashIndexModule} from './index/dash-index.route'
import { default as currentUserModule } from 'app/services/currentUser';
import { default as lodashModule } from 'app/services/loDash';

export default angular
	.module('Dash', [
		'ui.router',
		dashIndexModule.name,
		currentUserModule.name,
		lodashModule.name
	])
	.config(dashRouteConfig);


dashRouteConfig.$inject = ['$stateProvider']
function dashRouteConfig ($stateProvider) {
	$stateProvider.state('userdash', {
		url: '/dash',
		abstract: true,
		template: '<ui-view/>',
		resolve:{

			_: '_',

			CurrentUser: 'CurrentUser',

			activePartner: function(CurrentUser, _){
				console.log('CurrentUser', CurrentUser.user)
				return _.filter(CurrentUser.user.Partner, { deleted_at: null })[0]
			}
		}
	})
}
