import 'angular'
import {default as privatePostServiceModule} from 'app/services/api/privatePosts'

const _$STATESERVICE = new WeakMap()
const SCOPE = new WeakMap()

class CreatePostController{
	constructor($scope, PrivatePostsService, activePartner, CurrentUser, privatePosts, $state, FileUploaderService){
		_$STATESERVICE.set(this, $state)
		SCOPE.set(this, $scope)

		this.formFields = {}
		this.formFields.private = true
		this.PrivatePostsService = PrivatePostsService
		this.activePartner = activePartner
		this.user = CurrentUser.user
		this.privatePostList = privatePosts
		this.fileUpload = new FileUploaderService({url: PrivatePostsService.privatePostUrl})
	}

	submitForm(form){
		if( !form.$valid ) return

		let formFields = {
			title: form.title.$viewValue,
			content: form.content.$viewValue,
			relationship_id: this.activePartner.UserRelationship.relationship_id,
			user_id: this.user.id
		}

		if(this.fileUpload.queue.length)
			this.uploadFilesAndForm(formFields)
		else
			this.createPost(formFields)
	}

    uploadFilesAndForm(formBodyData){
    	this.fileUpload.setFormData( formBodyData )
    	this.fileUpload.uploadAllFiles({ relationship_id: formBodyData.relationship_id })
    		.then(newPrivatePost => { this.formPostSuccessHandler(newPrivatePost) })
			.catch( err => { console.log(err) })
    }

    createPost(formBodyData){
    	this.PrivatePostsService
			.privatePost()
			.save({ relationship_id: formBodyData.relationship_id }, formBodyData )
			.$promise
			.then(newPrivatePost => { this.formPostSuccessHandler(newPrivatePost) })
			.catch( err => { console.log(err) })
    }

    formPostSuccessHandler(newPrivatePost){
		this.privatePostList.push(newPrivatePost)
		_$STATESERVICE.get(this).go('userdash.index')
	}
}

CreatePostController.$inject = [
	'$scope',
	'PrivatePostsService',
	'activePartner',
	'CurrentUser',
	'privatePosts',
	'$state',
	'FileUploaderService'
]

export default CreatePostController
