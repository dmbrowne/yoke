import 'angular'
import 'angular-ui-router'
import {default as createPostController} from './create-post.controller'
import {default as fileUploadServiceModule} from 'app/services/fileUploader'

var createPostModule = angular.module('createPostModule', [
		'ui.router',
		fileUploadServiceModule.name
	])
	.config(createPostRouteConfig)

	createPostRouteConfig.$inject = ['$stateProvider']
	function createPostRouteConfig ($stateProvider){
		$stateProvider.state('userdash.index.createpost', {
			url: '/create-post',
			templateUrl: 'routes/dash/index/create-post/create-post.tpl.html',
			controller: createPostController,
			controllerAs: 'createPostCtrlr',
			resolve: {
				FileUploaderService: 'FileUploaderService'
			}
		})
	}

export default createPostModule;