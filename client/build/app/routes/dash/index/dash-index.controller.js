import 'angular';

const POSTSERVICE = new WeakMap()

class dashIndexController{
	constructor(PrivatePostsService, privatePosts, $state){
		POSTSERVICE.set(this, PrivatePostsService)
		this.privatePosts = privatePosts
		this.$state = $state
	}

	createpostState(){
		return this.$state.is('userdash.index.createpost')
	}
}

dashIndexController.$inject = ['PrivatePostsService', 'privatePosts', '$state']

export default dashIndexController
