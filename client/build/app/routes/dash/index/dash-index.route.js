import 'angular';
import 'angular-ui-router';
import { default as privatePostServiceModule } from 'app/services/api/privatePosts';
import { default as dashIndexController } from './dash-index.controller';
import { default as createPostRoute } from './create-post/create-post.route';


export default angular
	.module('DashIndex', [
		'ui.router',
		privatePostServiceModule.name,
		createPostRoute.name
	])
	.config(dashIndexRouteConfig);


dashIndexRouteConfig.$inject = ['$stateProvider']
function dashIndexRouteConfig ($stateProvider) {
	$stateProvider.state('userdash.index', {
		url: '',
		templateUrl: 'routes/dash/index/dash-index.tpl.html',
		controller: dashIndexController,
		controllerAs: 'dashIndexCtrlr',
		resolve: {
			PrivatePostsService: 'PrivatePostsService',

			privatePosts: function(activePartner, PrivatePostsService){
				return PrivatePostsService.privatePost().query({
					relationship_id: activePartner.UserRelationship.relationship_id
				}).$promise
			}
		}
	})
}
