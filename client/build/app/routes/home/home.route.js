import 'angular';
import 'angular-ui-router';
import { default as HomeController } from './home.controller';
// import { name as signUpControllerName } from './signup-index.controller';


export default angular.module('Home', ['ui.router'])
	.config(homeRouteConfig)
	// .controller('HomeController', HomeController);


homeRouteConfig.$inject = ['$stateProvider'];
function homeRouteConfig($stateProvider){
	$stateProvider.state('home',{
		url: "/",
		templateUrl: 'routes/home/home.tpl.html',
		controller: HomeController,
		controllerAs: 'homeCntrlr'
	});
}
