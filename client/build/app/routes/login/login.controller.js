import 'angular';

const controllerName = 'LoginController';
const USERSERVICE    = new WeakMap();
const STATESERVICE   = new WeakMap();
const MODAL          = new WeakMap();
const MODALINSTANCE  = new WeakMap();

class loginController{
	constructor(UserService, $state, $modal, CurrentUser, $timeout){
		USERSERVICE.set(this, UserService);
		STATESERVICE.set(this, $state);
		MODAL.set(this, $modal);
		this.authError = null;
		this.currentUser = CurrentUser
		this.$timeout = $timeout
	}

	login(form){
		if(!form.$valid) return;

		USERSERVICE.get(this)
			.getToken()
			.save({
				email: form.email.$viewValue,
				password: form.password.$viewValue
			})
			.$promise
			.then( (response) => { this.successHandler(response);  })
			.catch( err => { this.errorHandler(err) })
	}

	successHandler(res){
		this.currentUser.token = res.token

		USERSERVICE.get(this).users().get({
			user_id: res.user.id
		})
		.$promise
		.then( user => {
			console.log(user)
			this.currentUser.user = user
			this.currentUser.token = res.token

			// this.$timeout(()=>{	
			STATESERVICE.get(this).go('userdash.index')
			// })
		})
	}

	errorHandler(errorRespose){
		if ( errorRespose.status == 401 ){
			this.authFail(errorRespose.data.message);
			return;
		}else{
			MODALINSTANCE.set(this, MODAL.get(this)
				.open({
					animation: true,
					templateUrl: 'routes/login/login-error.tpl.html',
					size: 'md'
				})
			);
		}
	}

	authFail(message){
		if (message == "user doesn't exist")
			this.authError = 'noexist'

		if (message == "incorrect password")
			this.authError = 'password'
	}
}

loginController.$inject = ['UserService', '$state', '$modal', 'CurrentUser', '$timeout'];

export default loginController;
export { controllerName as name };
