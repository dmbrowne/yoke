import 'angular';
import 'angular-ui-router';
import 'ui-bootstrap';
import { default as loginController } from './login.controller';
import { name as loginCntlName } from './login.controller';
import { default as userService } from 'app/services/api/users';
import { default as currentUserModule } from 'app/services/currentUser';


var loginModule = angular
	.module('Login', [

		'ui.router',
		userService.name,
		'ui.bootstrap',
		currentUserModule.name,

	])
	.config(loginRouteConfig)
	.controller(loginCntlName, loginController);


export default loginModule;


loginRouteConfig.$inject = ['$stateProvider'];
function loginRouteConfig($stateProvider){
	$stateProvider.state('login', {
		url: "/login",
		templateUrl: 'routes/login/login.tpl.html',
		controller: loginCntlName,
		controllerAs: 'loginCntrlr'
	})
}
