import 'angular'

const _$STATESERVICE = new WeakMap()

class CreateNoteController{
	constructor(PrivatePostsService, activePartner, CurrentUser, $state, taOptions){
		_$STATESERVICE.set(this, $state)

		this.formFields = {}
		this.formFields.private = true
		this.PrivatePostsService = PrivatePostsService
		this.activePartner = activePartner
		this.user = CurrentUser.user
		this.toolbarOptions = [
			['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'pre', 'quote'],
			['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
			['html']
			// ['html', 'insertImage','insertLink', 'insertVideo', 'wordcount', 'charcount']
		]
	}

	submitForm(form){
		if( !form.$valid ) return

		let formFields = {
			title: form.title.$viewValue,
			content: form.noteContent.$viewValue,
			post_type: 'note',
			relationship_id: this.activePartner.UserRelationship.relationship_id,
			user_id: this.user.id
		}

		this.createPost(formFields)
	}

    createPost(formBodyData){
    	this.PrivatePostsService
			.privatePost()
			.save({ relationship_id: formBodyData.relationship_id }, formBodyData ).$promise
			.then(this.successHandler)
			.catch( err => { console.log(err) })
    }

    successHandler(){
		_$STATESERVICE.get(this).go('userdash.index')
	}
}

CreateNoteController.$inject = [
	'PrivatePostsService',
	'activePartner',
	'CurrentUser',
	'$state',
	'taOptions'
]

export default CreateNoteController
