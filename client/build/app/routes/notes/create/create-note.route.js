import 'angular'
import 'angular-ui-router'
import {default as createNoteController} from './create-note.controller'
// import {default as angularFileUpload} from 'angular-file-upload'
// import {default as fileUploadServiceModule} from 'app/services/fileUploader'
import 'rangy-core';
import 'rangy-selectionsaverestore';
import 'text-angular-sanitize';
import 'text-angular';

export default angular
	.module('createNoteModule', [
		'ui.router',
		'textAngular'
	])
	.config(createNoteRouteConfig)

createNoteRouteConfig.$inject = ['$stateProvider']
function createNoteRouteConfig ($stateProvider){
	$stateProvider.state('notes.create', {
		url: '/create',
		templateUrl: 'routes/notes/create/create-note.tpl.html',
		controller: createNoteController,
		controllerAs: 'createNoteCtrlr'
	})
}
