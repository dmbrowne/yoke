import 'angular';
import 'angular-ui-router';
import {default as createNoteModule} from './create/create-note.route';
import { default as privatePostServiceModule } from 'app/services/api/privatePosts';
import { default as currentUserServiceModule } from 'app/services/currentUser';

export default angular.module('NoteRoute', [
	'ui.router',
	createNoteModule.name,
	currentUserServiceModule.name,
	privatePostServiceModule.name
])
.config(noteRouteConfig)


noteRouteConfig.$inject = ['$stateProvider'];
function noteRouteConfig($stateProvider){
	$stateProvider.state('notes', {
		url: '/notes',
		abstract: true,
		template: '<div ui-view />',
		resolve: {

			_: '_',

			PrivatePostsService: 'PrivatePostsService',

			CurrentUser: 'CurrentUser',

			user: function(CurrentUser){
				return CurrentUser.user
			},

			allPartners: function(CurrentUser, _){
				return CurrentUser.user.Partner
			},

			activePartner: function(allPartners, _){
				return _.filter(allPartners, { UserRelationship: { deleted_at: null } })
			}
		}
	})
}
