import 'angular';

const USERSERVICE    = new WeakMap();
const MODAL          = new WeakMap();
const MODALINSTANCE  = new WeakMap();
const controllerName = 'JointSignupController';
const STATESERVICE   = new WeakMap();

class JointSignupController {
	constructor(UserService, $modal, $state){
		USERSERVICE.set(this, UserService);
		MODAL.set(this, $modal);
		this.formData = {};
		STATESERVICE.set(this, $state);
	}

	createJointAccount(form){
		if(!form.$valid) return;

		USERSERVICE.get(this)
			.users()
			.save({
				email: form.email.$viewValue,
				password: form.password.$viewValue,
				partner: {
					email: form.partnerEmail.$viewValue
				}
			})
			.$promise
			.then( responseObj => { this.accountCreateResponse(responseObj) })
			.catch( responseObj => { this.accountCreateResponse(responseObj) })
	}

	accountCreateResponse(response){
		if(response.code === 200)
			return STATESERVICE.get(this).go('login');

		if(response.code === 202)
			tplurl = 'routes/signup/joint-signup/user-already-exists.tpl.html';

		if(response.code === 203)
			tplurl = 'routes/signup/joint-signup/partner-already-exists.tpl.html';

		MODALINSTANCE.set(this, MODAL.get(this)
			.open({
				animation: true,
				templateUrl: tplurl,
				size: 'md'
			})
		)
	}
}

JointSignupController.$inject = ['UserService', '$modal', '$state'];
 
export default JointSignupController;
export { controllerName as name };
