import 'angular';
import 'angular-ui-router';
import 'ui-bootstrap';
import { default as jointsignUpController } from './joint-signup.controller';
import { name as jointsignUpControllerName } from './joint-signup.controller';
import { default as userService } from 'app/services/api/users';


var jointSignupModule = angular.module('JointSignup', ['ui.router', 'ui.bootstrap', userService.name])
	.config(jointSignupRoute)
	.controller(jointsignUpControllerName, jointsignUpController);

export default jointSignupModule;

jointSignupRoute.$inject = ['$stateProvider'];
function jointSignupRoute($stateProvider){
	$stateProvider.state('signup.joint',{
		url: "/joint",
		templateUrl: 'routes/signup/joint-signup/joint-signup.tpl.html',
		controller: jointsignUpControllerName,
		controllerAs: 'jointsignupCntrlr'
	});
}
