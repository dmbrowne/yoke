import 'angular';
import 'angular-ui-router';
import { default as signUpController } from './signup-index.controller';
import { name as signUpControllerName } from './signup-index.controller';


var signupIndexModule = angular.module('SignupIndex', ['ui.router'])
	.config(signupIndexRoute)
	.controller(signUpControllerName, signUpController);

export default signupIndexModule;

signupIndexRoute.$inject = ['$stateProvider'];
function signupIndexRoute($stateProvider){
	$stateProvider.state('signup.index',{
		url: "",
		templateUrl: 'routes/signup/signup-index/signup-index.tpl.html',
		controller: signUpControllerName,
		controllerAs: 'signupCntrlr'
	});
}
