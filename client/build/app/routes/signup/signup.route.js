import 'angular';
import 'angular-ui-router';
import {default as signupIndexModule} from './signup-index/signup-index.route';
import {default as jointSignupModule} from './joint-signup/joint-signup.route';
import {default as singleSignupModule} from './single-signup/single-signup.route';


export default angular.module('Signup',[ 'ui.router', signupIndexModule.name, jointSignupModule.name, singleSignupModule.name ])
	.config(signupRoute)

signupRoute.$inject = ['$stateProvider'];
function signupRoute($stateProvider){
	$stateProvider.state('signup',{
		abstract: true,
		url: "/signup",
		template: "<ui-view/>"
	});
}
