import 'angular';

const USERSERVICE    = new WeakMap();
const MODAL          = new WeakMap();
const MODALINSTANCE  = new WeakMap();
const controllerName = 'SingleSignupController';
const STATESERVICE   = new WeakMap();

class SingleSignupController {
	constructor(UserService, $modal, $state){
		USERSERVICE.set(this, UserService);
		MODAL.set(this, $modal);
		this.formData = {};
		STATESERVICE.set(this, $state);
	}

	createSingleAccount(form){
		if(!form.$valid) return;

		USERSERVICE.get(this)
			.users()
			.save({
				email: form.email.$viewValue,
				password: form.password.$viewValue
			})
			.$promise
			.then( responseObj => { this.accountCreateResponse(responseObj) })
			.catch( responseObj => { this.accountCreateResponse(responseObj) })
	}

	accountCreateResponse(response){
		if(response.code === 200)
			return STATESERVICE.get(this).go('login');

		if(response.code === 202){
			MODALINSTANCE.set(this, MODAL.get(this)
				.open({
					animation: true,
					templateUrl: 'routes/signup/single-signup/user-already-exists.tpl.html',
					size: 'md'
				})
			)
		}
	}
}

SingleSignupController.$inject = ['UserService', '$modal', '$state'];
 
export default SingleSignupController;
export { controllerName as name };
