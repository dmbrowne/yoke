import 'angular';
import 'angular-ui-router';
import 'ui-bootstrap';
import { default as singleSignUpController } from './single-signup.controller';
import { name as singleSignupControllerName } from './single-signup.controller';
import { default as userService } from 'app/services/api/users';


var singleSignUpmodule = angular.module('SingleSignup', ['ui.router', 'ui.bootstrap', userService.name])
	.config(singleSignupRoute)
	.controller(singleSignupControllerName, singleSignUpController);


export default singleSignUpmodule;


singleSignupRoute.$inject = ['$stateProvider'];
function singleSignupRoute($stateProvider){
	$stateProvider.state('signup.single',{
		url: "/single",
		templateUrl: 'routes/signup/single-signup/single-signup.tpl.html',
		controller: singleSignupControllerName,
		controllerAs: 'singleSignupCntrlr'
	});
}
