import 'angular';

class ApiConfig {
	constructor(){
	}

	protocol(){
		return 'http://';
	}

	domain(){
		return 'yoke.local:3000';
	}

	apiPath(){
		return '/api/v1/';
	}
}

export default ApiConfig;
