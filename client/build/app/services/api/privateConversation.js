import 'angular';
import 'angular-resource';
import { default as ApiConfig } from './api.config';

var apiConfig = new ApiConfig();

class PrivateConversation{

	constructor($resource){
		this.$resource = $resource;
	}

	conversation(convoIdOrRelationship){
		let extraRoutePath = '/id/:id';

		if(convoIdOrRelationship)
			extraRoutePath = '/' + convoIdOrRelationship + '/:id'

		return this.$resource(
			apiConfig.apiPath()  +
			'conversations' + extraRoutePath
		)
	}

	conversationByRelationshipId(){
		return this.conversation('relationship')
	}

	conversationById(){
		this.conversation('id')
	}
}

PrivateConversation.$inject = ['$resource'];

export default angular.module('PrivateConvoApiModule', ['ngResource'])
	.service('PrivateConversationService', PrivateConversation);
