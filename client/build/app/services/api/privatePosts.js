import 'angular';
import 'angular-resource';
import { default as ApiConfig } from './api.config';
import { default as currentUserModule } from 'app/services/currentUser';

var apiConfig = new ApiConfig();

class PrivatePosts{

	constructor($resource, CurrentUser){
		this.$resource = $resource
		this.currentUser = CurrentUser
	}

	// useful as we need to expose the url to use with other
	// services such as angular-file-upload
	get privatePostUrl(){
		return apiConfig.apiPath()  +
		'private-posts/:relationship_id/:post_id'
	}

	privatePost(userToken = this.currentUser.token){
		return this.$resource(this.privatePostUrl)
	}
}

PrivatePosts.$inject = ['$resource', 'CurrentUser'];

export default angular.module('PrivatePostApiModule', [])
	.service('PrivatePostsService', PrivatePosts);
