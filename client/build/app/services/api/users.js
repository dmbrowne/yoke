import 'angular';
import 'angular-resource';
import { default as ApiConfig } from 'app/services/api/api.config';
import { default as currentUserModule } from 'app/services/currentUser';

var apiConfig = new ApiConfig();

class UserService{
	constructor($resource, CurrentUser){
		this.$resource = $resource;
		this.currentUser = CurrentUser.user
	}

	users(){
		return this.$resource( apiConfig.apiPath() + 'users/:user_id' )
	}

	getToken(){
		return this.$resource( apiConfig.apiPath() + 'login' )
	}

	verify(){
		return this.$resource( apiConfig.apiPath() + 'verifyuser/:user_id', {}, {
			'update': { method: 'PUT'}
		})
	}
}

UserService.$inject = ['$resource', 'CurrentUser'];

export default angular.module('UserAPI', ['ngResource', currentUserModule.name])
	.service('UserService', UserService);
