import 'angular'
import { default as lodashModule } from 'app/services/loDash';

const CRRNTUSR = new WeakMap()
const STORAGE  = new WeakMap()
let lodash;

class CurrentUser{
	constructor(locker, _){
		STORAGE.set(this, locker)

		lodash = _

		if( STORAGE.get(this).get('current-user') )
			CRRNTUSR.set(this, STORAGE.get(this).get('current-user'))
		else
			CRRNTUSR.set(this, {})

		// if( STORAGE.get(this).get('user') === undefined ) CRRNTUSR.set(this, {})

	}

	get user(){
		return (lodash.size( CRRNTUSR.get(this).user ) ? CRRNTUSR.get(this).user : undefined)
	}

	set user(usr){
		CRRNTUSR.get(this).user = usr
		STORAGE.get(this).put('current-user', CRRNTUSR.get(this))

		return CRRNTUSR.get(this).user
	}

	get token(){
		return CRRNTUSR.get(this).token
	}

	set token(tken){
		CRRNTUSR.get(this).token = tken
		STORAGE.get(this).put('current-user', CRRNTUSR.get(this))

		return CRRNTUSR.get(this).token
	}

	addUserDetails(newDetails){
		let user = CRRNTUSR.get(this).user

		newDetails.forEach(function(val, key){
			user.key = val
		})

		STORAGE.get(this).put('current-user', CRRNTUSR.get(this))

		return CRRNTUSR.get(this).user
	}
}

CurrentUser.$inject = ['locker', '_']

var currentUserService = angular.module('currentUserModule', ['angular-locker', lodashModule.name])
	.service('CurrentUser', CurrentUser)

export default currentUserService
