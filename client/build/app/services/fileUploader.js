import 'angular'
import {default as angularFileUpload} from 'angular-file-upload'
import {default as lodashModule} from 'app/services/loDash'
import { default as currentUserModule } from 'app/services/currentUser';


export default angular.module('FileUploaderServiceModule', [
        angularFileUpload.name,
        lodashModule.name,
        currentUserModule.name
    ])
    .factory('FileUploaderService', YkFileUploaderInstance)


YkFileUploaderInstance.$inject = ['$rootScope', 'FileUploader', '_', 'CurrentUser', '$q']
function YkFileUploaderInstance($rootScope, FileUploader, _, CurrentUser, $q) {
    let self
    FileUploader.inherit(YkFileUploader, FileUploader)

    YkFileUploader.prototype.onAfterAddingFile  = afterAddingFile
    YkFileUploader.prototype.onBeforeUploadItem = beforeUploadItem
    YkFileUploader.prototype.setFormData        = formdata
    YkFileUploader.prototype.setUrl             = urlSetter
    YkFileUploader.prototype.uploadAllFiles     = uploadAllFiles
    YkFileUploader.prototype.onCompleteAll      = uploadsComplete
    YkFileUploader.prototype.onErrorItem        = uploadError
    YkFileUploader.prototype.onSuccessItem = function(item, response, status, headers){
        console.log(item, response, status)
    }

    return YkFileUploader;


    function YkFileUploader(options) {
        self = this
        let uploaderOptions = angular.extend(
            {
                headers: {'Authorization':'Bearer '+ CurrentUser.token}
            },
            options
        )
        YkFileUploader.super_.call(self, uploaderOptions);
        console.log(self)
    }

    function afterAddingFile(item){
        item.croppedImage = '';
        let reader = new FileReader();

        reader.onload = (event) => {
            $rootScope.$apply(() => {
                item.image = event.target.result;
            })
        }

        reader.readAsDataURL(item._file);
    }

    function beforeUploadItem(item) {
        item.url = self.url

        if(item.croppedImage === '') return

        let blob = dataURItoBlob(item.croppedImage);
        item._file = blob;
    }

    function formdata(dataObj){
        self.formData = [dataObj]

        _.forEach(self.queue, (fileItem) => {
            fileItem.formData = [dataObj]
        })
    }

    function urlSetter(newUrl){
        self.url = newUrl

        _.forEach(self.queue, (fileItem) => {
            fileItem.url = self.url
        })

        return self.url
    }

    function dataURItoBlob(dataURI) {
        let binary = atob(dataURI.split(',')[1]);
        let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        let array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
    }

    function uploadAllFiles(urlParams){
        let deferred = $q.defer()

        _.forEach(urlParams, (val, key) => {
            let newtext= ''
            if(self.url.indexOf(':' + key))
                newtext = self.url.replace(':' + key, val)
            else
                newtext = self.url + (self.url.indexOf('?') ? '&' : '?') + key + '=' + val

            self.url = newtext
        })

        let namedParamExp = new RegExp('(/:)\\w+\\b');
        let newUrl = self.url.replace(namedParamExp, '');
        self.url = newUrl

        self.uploadAll()


        // some sugar to make it return the same as an $resource promise
        deferred.$promise = deferred.promise
        self.deferred = deferred
        return self.deferred.promise
    }

    function uploadsComplete(){
        self.deferred.resolve()
    }

    function uploadError(item, response, status, headers){
        self.deferred.reject({
            item: item,
            response: response,
            status: status
        })
    }
}
