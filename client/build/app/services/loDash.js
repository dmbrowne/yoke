import 'angular'

/* Needlessly complicated,
 * consider turning into a normal function?
 */
class LoDash{
	constructor($window){
		this._ = $window._
		delete($window._)
	}

	static lodashFactory($window){
		return new LoDash($window)._
	}
}

LoDash.lodashFactory.$inject = ['$window']

export default angular.module('yokeLodashService', []).service('_', LoDash.lodashFactory)
