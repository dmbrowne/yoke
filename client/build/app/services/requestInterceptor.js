import 'angular'
import {default as currentUserModule} from 'app/services/currentUser'
import {default as lodash} from 'app/services/loDash'

export default angular
    .module('yokeTokenInterceptorFactoryModule', ['ngResource', currentUserModule.name, lodash.name])
    .factory('TokenRequestInterceptor', TokenRequestInterceptor);


TokenRequestInterceptor.$inject = ['$q', 'CurrentUser', '_']

function TokenRequestInterceptor ($q, CurrentUser, _){
    var that = this
    this.currentUser  = CurrentUser
    return {
        request: function(config) {
            config.headers = config.headers || {};

            if(CurrentUser.token)
                config.headers.Authorization = 'Bearer ' + CurrentUser.token

            return config;
        },

        response: function(response) {
            /*if (response.status === 403 || response.status === 401) {
                // insert code to redirect to custom unauthorized page
            }*/
            return response || $q.when(response);
        }
    }
}

