import 'angular';
import { default as currentUserServiceModule } from 'app/services/currentUser';

const SOCKET = new WeakMap();
const ROOTSCOPE = new WeakMap();
// const ISAUTHENTICATED = new Map();
const CRRNTUSR = new WeakMap();

var isAuthenticated;
var $q;

class SocketIO{
	constructor($rootScope, CurrentUser, $qService){
		ROOTSCOPE.set(this, $rootScope)
		CRRNTUSR.set(this, CurrentUser)
		$q = $qService
	}

	on(eventName, callback) {
		SOCKET.get(this).on(eventName, () => {
			let args = arguments;
			ROOTSCOPE.get(this).$apply(() => {
				callback.apply(SOCKET.get(this), args);
			})
		})
	}

	emit(eventName, data) {
		let deferred = $q.defer()

		if(isAuthenticated || eventName === 'authentication'){

	      	SOCKET.get(this).emit(eventName, data, (err) => {

	        	ROOTSCOPE.get(this).$apply(() => {
	        		if(err)
	        			deferred.reject(err)
	        		else
	        			deferred.resolve()
	        	})
	      	})
	    }
		else{
	    	SocketIO.authenticate().then(()=>{
	    		this.emit(eventName, data, callback)
	    	})
	    }

	    return deferred.promise
    }

    sendMessage(messageOpts){
    	let sendSocketMessage = $q.defer()

    	if(!messageOpts.room_id || !messageOpts.message) sendSocketMessage.reject('both roomid and message need to be present in the message object');

    	let messageObj = {
    		roomId: messageOpts.room_id,
    		message: messageOpts.message
    	}

    	this.emit('newMessage', messageObj)
    		.then( () => { sendSocketMessage.resolve() })
    		.catch( err => { sendSocketMessage.reject(err) })

    	return sendSocketMessage.promise
    }

    joinRoom(roomId){
    	if(!roomId) return;
    	return this.emit('join room', roomId)
    }

    connect(namespace = '/'){
    	let deferred = $q.defer();

        if ( SOCKET.get(this) && SOCKET.get(this).connected && SOCKET.get(this).nsp === namespace ){
            this.authenticate()
                .then( () => { deferred.resolve() } )
                .catch( () => { deferred.reject() } )
        }else{
            SOCKET.set(this, io('localhost:3000' + namespace))
        }

    	this.on('connect', () => {
			this.authenticate()
                .then( () => { deferred.resolve() } )
                .catch( () => { deferred.reject() } )
		})

        return deferred.promise
    }

    authenticate(newAuth){
        let self                 = this.authenticate
        let authenticatePromise  = self.authenticationDefer
        self.authenticationDefer = ( newAuth ? $q.defer : authenticatePromise || $q.defer() )

        self.resolve = function(){
            isAuthenticated = true
            self.authenticationDefer.resolve()
        }

        self.reject = function(){
            isAuthenticated = false
            self.authenticationDefer.reject()
        }

        // if(isAuthenticated) self.authenticationDefer will already be resolved to need to do anything here

        if(!authenticatePromise)
            this.emit('authentication', {user_id: CRRNTUSR.get(this).user.id, token: CRRNTUSR.get(this).token})

		this.on('authenticated', () => { self.resolve() })

        this.on('authentication:Error', () => { self.reject() })

    	return self.authenticationDefer.promise
    }
}

SocketIO.$inject = ['$rootScope', 'CurrentUser', '$q'];

export default angular.module('SocketService', []).service('SocketIO', SocketIO)
