System.config({

	// set all requires to "js" for library code
	baseURL: '/js/',


	// set "js/app" as an exception for our application code
	paths: {
	  'app/*': '/js/app/*.js'
	},

	meta: {

		'angular':                       { format: 'global', exports: 'angular' },

		'angular-ui-router':             { deps: ['angular'] },

		'angular-resource':              { deps: ['angular'] },

		'angular-file-upload':           { deps: ['angular'] },

		'text-angular-sanitize':         { deps: ['angular'] },

		'rangy-core':                    { format: 'global', deps: ['angular'] },

		'rangy-selectionsaverestore':    { format: 'global', deps: ['angular'] },

		'text-angular':                  { deps: ['angular', 'rangy-core', 'rangy-selectionsaverestore'] },

		'ui-bootstrap':                  { deps: ['angular'] },

		'angular-locker':                { deps: ['angular'] }
	},

	map: {
		// 'moduleName' : 'file/location/including/actualFileWithoutExtension'
		// 'app': 'js/app/app',
		'angular':                    'angular',
		'angular-ui-router':          'angular-ui-router',
		'angular-resource':           'angular-resource',
		'angular-file-upload':        'angular-file-upload',
		'text-angular-sanitize':      'textAngular-sanitize',
		'rangy-core':                 'rangy-core',
		'rangy-selectionsaverestore': 'rangy-selectionsaverestore',
		// 'text-angular-rangy':      'textAngular-rangy.min',
		'text-angular':               'textAngular.min',
		'ui-bootstrap':               'ui-bootstrap-tpls',
		'appTemplates':               'appPartials.tpl',
		'angular-locker':             'angular-locker'
	}
})
