// Karma configuration
// Generated on Wed Apr 22 2015 16:03:02 GMT+0000 (UTC)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../../',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'jspm'],

    // plugins: ['karma-jasmine', 'karma-firefox-launcher', 'karma-systemjs'],


    // list of files / patterns to load in the browser
    files: [
        'client/public/js/traceur-runtime.js'
    ],


    // list of files to exclude
    exclude: [
    ],

    jspm: {
        config: 'client/public/js/app/systemJsConfig.js',
        loadFiles: ['client/public/js/app/**/*.spec.js'],
        packages: 'jspm_packages/',
        baseURL: '/',
        serveFiles: [
            'client/public/**/*.js',
            'vendor/bower_components/angular-mocks/*.js'
        ],
        paths: {
            'angular-mocks': 'vendor/bower_components/angular-mocks/angular-mocks.js',
            '*': 'client/public/js/*.js'
        }
    },

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },

    proxies: {
        'build/': '/base/client/public/js/',
        '/base/client/public/js/client/public/js/': '/base/client/public/js',
        '/js/app/': '/base/client/public/js/app/',
        // '/base/angular.js': '/base/client/public/js/angular.js',
        // '/base/angular-resource.js': '/base/client/public/js/angular-resource.js',
        // '/base/appPartials.tpl.js': '/base/client/public/js/appPartials.tpl.js',
        // '/base/angular-ui-router.js': '/base/client/public/js/angular-ui-router.js',
        // '/base/ui-bootstrap-tpls.js': '/base/client/public/js/ui-bootstrap-tpls.js',
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'html'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
