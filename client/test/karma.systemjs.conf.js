// Karma configuration
// Generated on Wed Apr 22 2015 16:03:02 GMT+0000 (UTC)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../../',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'systemjs'],

    // plugins: ['karma-jasmine', 'karma-phantomjs-launcher', 'karma-systemjs'],


    // list of files / patterns to load in the browser
    files: [],


    // list of files to exclude
    exclude: [
    ],

    systemjs: {
        // Path to your SystemJS configuration file
        configFile: 'client/public/js/app/systemJsConfig.js',


        // File patterns for your application code, dependencies, and test suites
        files: [
            'client/public/js/**/*.js',
            'client/build/app/**/*.spec.js'
        ],

        // SystemJS configuration specifically for tests, added after your config file.
        // Good for adding test libraries and mock modules
        config: {
            transpiler: 'traceur',

            baseURL: '/',

            paths: {
                'angular-mocks': '/vendor/bower_components/angular-mocks/angular-mocks.js',
                '/*': 'client/public/js/*.js',

                // 'es6-module-loader': 'client/public/js/es6-module-loader.js'
            }
        },

        // Specify the suffix used for test suite file names.  Defaults to .test.js, .spec.js, _test.js, and _spec.js
        testFileSuffix: '.spec.js'
    },

    proxy:{
        // '/yoke/': '/yoke/client/public/js/'
    },

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
