exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',


	// ---------------------------------------------------------------------------
	// ----- What tests to run ---------------------------------------------------
	// ---------------------------------------------------------------------------

	// Spec patterns are relative to the location of this config.
	specs: ['../**/*.e2e.js']

	// Patterns to exclude.
	exclude: [],

	// Alternatively, suites may be used. When run without a command line
	// parameter, all suites will run. If run with --suite=smoke or
	// --suite=smoke,full only the patterns matched by the specified suites will
	// run.
	/*suites: {
		smoke: 'spec/smoketests/*.js',
		full: 'spec/*.js'
	},*/


	// ---------------------------------------------------------------------------
	// ----- How to set up browsers ----------------------------------------------
	// ---------------------------------------------------------------------------
	//
	// Protractor can launch your tests on one or more browsers. If you are
	// testing on a single browser, use the capabilities option. If you are
	// testing on multiple browsers, use the multiCapabilities array.

	// For a list of available capabilities, see
	// https://github.com/SeleniumHQ/selenium/wiki/DesiredCapabilities
	//
	// In addition, you may specify count, shardTestFiles, and maxInstances.
	capabilities: {
		browserName: 'chrome',

		// Maximum number of browser instances that can run in parallel for this
		// set of capabilities. This is only needed if shardTestFiles is true.
		// Default is 1.
		maxInstances: 1,

		// Additional spec files to be run on this capability only.
		specs: [],

		// Spec files to be excluded on this capability only.
		exclude: []
	},

	// ---------------------------------------------------------------------------
  // ----- Global test information ---------------------------------------------
  // ---------------------------------------------------------------------------
  //
  // A base URL for your application under test. Calls to protractor.get()
  // with relative paths will be prepended with this.
  // baseUrl: 'http://localhost:9876',

  // CSS Selector for the element housing the angular app - this defaults to
  // body, but is necessary if ng-app is on a descendant of <body>.
  rootElement: 'body',


  // ---------------------------------------------------------------------------
  // ----- The test framework --------------------------------------------------
  // ---------------------------------------------------------------------------

  // Test framework to use. This may be one of:
  //  jasmine, jasmine2, cucumber, mocha or custom.
  //
  // Jasmine and Jasmine2 are fully supported as test and assertion frameworks.
  // Mocha and Cucumber have limited support. You will need to include your
  // own assertion framework (such as Chai) if working with Mocha.
  framework: 'jasmine2',

  // Options to be passed to jasmine2.
  //
  // See https://github.com/jasmine/jasmine-npm/blob/master/lib/jasmine.js
  // for the exact options available.
  jasmineNodeOpts: {
    // If true, print colors to the terminal.
    showColors: true,
    // Default time to wait in ms before a test fails.
    defaultTimeoutInterval: 30000
  },
}
