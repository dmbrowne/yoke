/**
 * This file/module contains all configuration for the build process.
 */
var gulpConfig = {
  /**
   * The `build_dir` folder is where our projects are compiled during
   * development and the `compile_dir` folder is where our app resides once it's
   * completely built.
   */
  build_dir: 'client/build',
  public_dir: 'client/public',
  production_dir: 'client/bin',

  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `build/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`build/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    js: [ 'client/build/**/*.js', '!client/build/**/*.e2e.js', '!client/build/**/*.spec.js', '!client/build/**/*.tpl.js' ],
    karma: [ 'client/build/**/*.spec.js' ],
    protractor: [ 'client/build/**/*.e2e.js' ],

    atpl: 'client/build/app/**/*.tpl.html',
    atplBase: 'client/build/app',
    atplOutputName: 'appTemplates',

    ctpl: 'client/build/app/common/**/*.tpl.html',
    ctplOutputName: 'commonTemplates',
    ctplBase: 'client/build/app',

    assets: 'client/build/assets/**/*',

    html: 'client/build/index.html',
    scss_dir: 'client/build/sass',
    scss: 'client/build/sass/style.scss'
  },

  /**
   * This is a collection of files used during testing only.
   */
  test_files: {
    spec: ['client/build/**/*.spec.js'],
    deps: {
      'angular-mocks' : 'vendor/bower_components/angular-mocks/angular-mocks.js'
    }
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   *
   * The `vendor_files.assets` property holds any assets to be copied along
   * with our app's assets. This structure is flattened, so it is not
   * recommended that you use wildcards.
   */
  vendor_files: {
    js: [
      'node_modules/socket.io/node_modules/socket.io-client/socket.io.js',
      'vendor/bower_components/jquery/dist/jquery.min.js',
      'vendor/bower_components/arrive/src/arrive.js',
      'vendor/bower_components/bootstrap-material-design/dist/js/material.js',
      'vendor/bower_components/bootstrap-material-design/dist/js/ripples.js',
      // 'vendor/bower_components/moment/min/moment.min.js',
      'vendor/bower_components/cropper/dist/cropper.min.js',
      'node_modules/lodash/dist/lodash.min.js',

      'vendor/bower_components/angular/angular.js',
      'vendor/bower_components/ui-router/release/angular-ui-router.js',
      'vendor/bower_components/angular-file-upload/angular-file-upload.js',
      'vendor/bower_components/angular-resource/angular-resource.js',
      'vendor/bower_components/angular-locker/dist/angular-locker.js',
      // 'vendor/bower_components/angular-bootstrap/ui-bootstrap.js',
      'vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'vendor/bower_components/rangy/rangy-core.js',
      'vendor/bower_components/rangy/rangy-selectionsaverestore.js',
      'vendor/bower_components/textAngular/dist/textAngular-sanitize.js',
      'vendor/bower_components/textAngular/dist/textAngular.min.js'
    ],
    devOnlyJs: {
      'es6-module-loader.js' : 'node_modules/es6-module-loader/dist/es6-module-loader.js',
      'traceur-runtime.js' : 'node_modules/traceur/bin/traceur-runtime.js',
      'system.js' : 'node_modules/systemjs/dist/system.js'
    },
    css: [
      'vendor/bower_components/cropper/dist/cropper.min.css',
      'vendor/bower_components/textAngular/dist/textAngular.css'
    ],
    assets: {
      'bootstrap' : [ 'vendor/bower_components/bootstrap-sass/assets/fonts/bootstrap/**' ],
      'font-awesome' : [ 'vendor/bower_components/font-awesome/fonts/**' ],
      'bootstrap-material' : [ '!vendor/bower_components/bootstrap-material-design/fonts/*.txt', 'vendor/bower_components/bootstrap-material-design/fonts/*' ]
    }
  },
};

module.exports = gulpConfig;
