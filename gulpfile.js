var gulp          = require('gulp')
var nodemon       = require('gulp-nodemon')
var rename        = require('gulp-rename')
var jshint        = require('gulp-jshint')
var jshintStylish = require('jshint-stylish')
var concat        = require('gulp-concat')
var uglify        = require('gulp-uglify')
var minifyhtml    = require('gulp-minify-html')
var html2js       = require('gulp-html2js')
var flatten       = require('gulp-flatten')
var $q            = require('q')
var browserSync   = require('browser-sync').create();
var notify        = require('gulp-notify')
var plumber       = require('gulp-plumber')
var sass          = require('gulp-sass')
var inject        = require('gulp-inject')
var _             = require('lodash')
var sourcemaps    = require('gulp-sourcemaps')
var traceur       = require('gulp-traceur')
var babel         = require('gulp-babel')
var gulpWatch     = require('gulp-watch')
var buildConfig   = require('./gulpConfig.js')
var del           = require('del')
var runSequence   = require('run-sequence')
var preProccess   = require('gulp-preprocess')
var rename        = require("gulp-rename")
var path          = require('path')
var karma         = require('karma').server
var env;

/* =======================
** JAVASCRIPT
** =======================
 */
//Lint custom application js files
gulp.task('lint', lintApplicationJs)

//create template cache from app & common folder and export to public dir
gulp.task('ngTemplateCache-app', appTemplateCache)
gulp.task('ngTemplateCache-common', commonTemplateCache)

// Concatenate custom angular app modules into one js file in public dir
gulp.task('copy-app-js', ['lint'], copyAppJs)
gulp.task('application-js', ['lint'], traceurCopyAppJs)
gulp.task('app-tests', copyTests)
gulp.task('concat-app-js', ['copy-app-js'], concatApplicationJs)

gulp.task('copy-vendorjs', copyVendorJS)
gulp.task('copy-concat-vendorjs', copyandConcatVendorJS)

//uglify concatenated js file
gulp.task('minify-js', ['concat-app-js', 'copy-concat-vendorjs'], minifyJs)


/* =======================
** CSS
** =======================
 */
gulp.task('copy-vendor-css', copyVendorCss)
gulp.task('sass', sassDev)
gulp.task('sass:prod', ['sass', 'copy-vendor-css'], sassProd)


/* =======================
** ASSETS
** =======================
 */
gulp.task('copy-app-assets', copyAppAssets)
gulp.task('copy-vendor-assets', copyVendorAssets) // vendor assets MAY contain css also


/* =======================
** INDEX HANDLING
** =======================
*/
gulp.task('index-inject', [
	'application-js',
	'copy-vendorjs',
	'ngTemplateCache-app',
	'ngTemplateCache-common',
	'sass',
	'copy-vendor-css',
	'copy-vendor-assets'
], devIndexInject)

gulp.task('index-inject:prod', [
	'concat-app-js',
	'copy-concat-vendorjs',
	'ngTemplateCache-app',
	'ngTemplateCache-common',
	'copy-vendor-css',
	'copy-vendor-assets'
], indexInject)



/* =======================
** MISC
** =======================
 */
gulp.task('clean', function(cb){
	del(buildConfig.public_dir)
	setTimeout(cb, 1000);
})



/* =======================
** NODE/SERVER
** =======================
 */
gulp.task('node', startNodeServer)
gulp.task('browser-sync', syncReload)
gulp.task('server-lint', lintApiJs)
gulp.task('lint-gulpfile', lintGupfiles)
gulp.task('karma-test', karmaSingleRun)




/* =======================
** GULP RUN TASKS
** =======================
 */
gulp.task('watch', watchTask)

gulp.task('dev', ['clean'], function(){
	runSequence(['sass','copy-app-assets'],'index-inject')
})

gulp.task('default', ['dev'])

gulp.task('test', ['clean'], function (){
	runSequence('dev', 'karma-test')
})

gulp.task('prod', ['dev'], function(){
	env = 'prod'
	runSequence(['sass:prod','copy-app-assets'],'index-inject:prod')
})

gulp.task('serve', ['dev'], function(){
	runSequence('node', ['browser-sync', 'watch'])
})










/* ==============================
 * ====================
 * HTML index handling
 * ======
 * ===
 */

		 // inject only css and assets becuase the js' are handled though es6 modules
		function devIndexInject() {
			var indexHtml     = gulp.src('client/build/index.html')
			var assetSources  = gulp.src(['assets/**/*.css', 'css/*.css'], { cwd: buildConfig.public_dir, read: false})

			return indexHtml
				.pipe(preProccess({context: {
					NODE_ENV: 'dev'
				}}))
				.pipe(inject(assetSources, {
					addRootSlash: true,
					starttag: '<!-- inject:head:css -->'
				}))
				.pipe(gulp.dest(buildConfig.public_dir));
		}

		// vendor js is contenated, so insert concatenated js plus app js
		function indexInject() {
			var indexHtml     = gulp.src('client/build/index.html')
			var assetSources  = gulp.src('assets/css/*', { cwd: buildConfig.production_dir, read: false})
			var scriptsources = gulp.src([
					'js/app.js'
				], {read: false, cwd: buildConfig.production_dir})

			return indexHtml
				.pipe(preProccess({context: {
					NODE_ENV: 'production'
				}}))
				.pipe(inject(assetSources, {
					addRootSlash: true,
					starttag: '<!-- inject:head:css -->'
				}))
				.pipe(inject(scriptsources, {
					addRootSlash: true
				}))
				.pipe(gulp.dest(buildConfig.production_dir));
		}



/* ==============================
 * ====================
 * CSS, Sass and Styling
 * ======
 * ===
 */
		function sassDev() {
			return gulp.src('client/build/sass/**/*.scss')
				.pipe(plumber())
				.pipe(sourcemaps.init())
				.pipe(
					sass.sync({
						includePaths: [
							buildConfig.dev_dir + '/app',
							buildConfig.dev_dir + '/common',
							'vendor/downloaded',
							'vendor/bower_components'
						],
						imagePath: (env === 'prod' ? 'client/public/assets/img' : 'client/public/assets/img'),
						outputStyle: (env === 'prod' ? 'compressed' : 'expanded')
					})
					.on('error', sass.logError)
				)
				.pipe(sourcemaps.write('.'))
				// .pipe(plumber.stop())
				.pipe(gulp.dest(buildConfig.public_dir + '/css'));
		}

		function sassProd() {
			return gulp.src([buildConfig.public_dir + '/assets/css/vendorCss.css', buildConfig.public_dir + '/css/style.css'])
				.pipe(concat('styles.css'))
				.pipe(gulp.dest(buildConfig.production_dir + '/css'));
		}

		function copyVendorCss() {
			if(buildConfig.vendor_files.css.length <= 0)
				return

			return gulp.src(buildConfig.vendor_files.css)
				.pipe(concat('vendorCss.css'))
				.pipe(gulp.dest(buildConfig.public_dir + '/assets/css'));
		}



/* ==============================
 * ====================
 * Assets
 * ======
 * ===
 */
		 // app imgs & fonts
		function copyAppAssets() {
			return gulp.src(buildConfig.app_files.assets)
				.pipe(gulp.dest('client/public/assets'))
		}

		// assets from vendor
		function copyVendorAssets() {
			_.forEach(buildConfig.vendor_files.assets, function(files, folderName){
				gulp.src(files)
					.pipe(flatten())
					.pipe(gulp.dest('client/public/assets/'+folderName))
			})
		}


/* ==============================
 * ====================
 * js Hinting & linting
 * ======
 * ===
 */
		function lintGupfiles(){
			return gulp.src(['gulpfile.js', 'gulpConfig.js'])
				.pipe(jshint())
				.pipe(jshint.reporter(jshintStylish))
				.pipe(jshint.reporter('fail'))
		}

		function lintApiJs(){
			return gulp.src(['!server/test/**/*', 'server/**/*.js'])
				.pipe(jshint())
				.pipe(jshint.reporter(jshintStylish))
				.pipe(jshint.reporter('fail'))
		}

		function lintApplicationJs(){
			return gulp.src(buildConfig.app_files.js)
				.pipe(jshint())
				.pipe(jshint.reporter(jshintStylish))
				.pipe(jshint.reporter('fail'))
		}


/* ==============================
 * ====================
 * Application js
 * ======
 * ===
 */
		// Dev Task:
		// convert app js files  to es5 with traceur and copy them over and keep their folder structure in public dest folder
		function traceurCopyAppJs(){
			return gulp.src(buildConfig.app_files.js)
				// .pipe(sourcemaps.init())
		        .pipe( traceur() )
				// .pipe(sourcemaps.write('.'))
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}

		function copyTests(){
			return gulp.src(buildConfig.test_files.spec)
				// .pipe(sourcemaps.init())
		        .pipe( traceur() )
				// .pipe(sourcemaps.write('.'))
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}

		// Prod Task:
		// (this task is to all systemJS-builder to create a js file that doesn't need systemJS,
		//	traceur, es6-module-loader or any other transpiler )
		//
		// copies over app js files into public dest folder without converting to
		// es5 (files are left as es6). this overwrites any previous files in the
		// public dest folder that were converted to es5
		function copyAppJs(){
			return gulp.src(buildConfig.app_files.js)
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}

		// Prod Task:
		// transform application js in public folder from es6 to es5, and make it independent of
		// system JS, es-module-loader, traceur or anything similar
		function concatApplicationJs(){
			var systemjsBuilder = require('systemjs-builder')
			var builder         = new systemjsBuilder()
				builder.reset()

			return builder.loadConfig(buildConfig.public_dir+'/js/app/systemJsConfig.js')
				.then(function() {
					builder.loader.baseURL = path.resolve(buildConfig.public_dir+'/js/');
					return builder.buildSFX('app/appBootstrap', buildConfig.production_dir+'/js/app.js', { runtime: true })
				})
		}


/* ==============================
 * ====================
 * Vendor Js
 * ======
 * ===
 */
		// DEV task: copy over all vendor javascript files without concatenation
		function copyVendorJS() {
			var devOnlyJs = buildConfig.vendor_files.devOnlyJs

			_.forEach(devOnlyJs, function(filepath, filename){
				gulp.src(filepath)
					.pipe(rename(filename))
					.pipe(gulp.dest(buildConfig.public_dir+'/js'))
			})

			gulp.src(buildConfig.vendor_files.js)
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}

		// PROD task: copy over all vendor javascript files and concatenate
		function copyandConcatVendorJS() {
			return gulp.src(buildConfig.vendor_files.js)
				.pipe(concat('vendor.js'))
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}


/* ==============================
 * ====================
 * Template Cache
 * ======
 * ===
 */
		function appTemplateCache(){
			return gulp.src(buildConfig.app_files.atpl)
				.pipe(html2js({
					outputModuleName: 'appTemplates',
					useStrict: true,
					base: buildConfig.app_files.atplBase
				}))
				.pipe(concat('appPartials.tpl.js'))
				.pipe( gulp.dest(buildConfig.public_dir+'/js') )
		}

		function commonTemplateCache(){
			return gulp.src(buildConfig.app_files.ctpl)
				.pipe(html2js({
					outputModuleName: 'commonTemplates',
					useStrict: true,
					base: buildConfig.app_files.ctplBase
				}))
				.pipe(concat('commonAppPartials.tpl.js'))
				.pipe(gulp.dest(buildConfig.public_dir+'/js'))
		}



/* ==============================
 * ====================
 * Misc
 * ======
 * ===
 */
function karmaSingleRun(done) {
	karma.start({
		configFile: __dirname + '/client/test/karma.jspm.conf.js',
		singleRun: true
	},
	function(){
		done();
	})
}

 function syncReload(){
 	if(browserSync.active) return

 	return browserSync.init({
        proxy: "http://localhost:3000",
        port: 3001,
        reloadDebounce: 3000,
		open: false // Stop the browser from automatically opening
    })
 }

 function watchTask() {
	gulp.watch(['gulpfile.js', 'gulpConfig.js'], ['lint-gulpfile'])

	gulp.watch('client/build/index.html', ['index-inject'])
		.on('all', browserSync.reload)

	gulp.watch([buildConfig.app_files.atpl, buildConfig.app_files.ctpl], ['ngTemplateCache-app', 'ngTemplateCache-common'])

	gulp.watch('server/**/*.js', ['server-lint'])

	gulp.watch('client/build/sass/**/*.scss', ['sass'])

	gulp.watch('client/build/assets/**/*', ['copy-app-assets'])

	gulp.watch(buildConfig.app_files.js, ['application-js'])
	// gulp.watch(buildConfig.app_files.js, function(file){
	// 	return gulp.src(file.path)
	// 		.pipe( traceur() )
	// 		.pipe(gulp.dest(buildConfig.public_dir+'/js'))
	// })

	gulp.watch(['!client/public/index.html', '!client/public/**/*.css','client/public/**/*'], function(file){
		browserSync.reload(file.path)
	})

	gulp.watch('client/public/**/*.css', function(file){
		gulp.src(file.path)
			.pipe(browserSync.stream({once: true}))
	})

	// gulpWatch('client/public/**/*', function(){
		// browserSync.reload({
			// reloadDebounce: 3000
		// });
	// })
}

function minifyJs(){
	return gulp.src([
			'client/public/vendor.js',
			'client/public/custom.js',
			'client/public/commonAppPartials.tpl.js',
			'client/public/appPartials.tpl.js'
		])
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(rename('all.min.js'))
		.pipe(gulp.dest(buildConfig.public_dir))
}


function startNodeServer(done) {
	return nodemon({
		script: 'app.js',
		ext: 'js',
		watch: ['server/**/*', './app.js', './gulpfile.js', './gulpConfig.js'],
		env: { 'NODE_ENV': 'development' },
		tasks: ['server-lint']
	})
	.on('start', function() {
		setTimeout(function() {
			done()
		}, 2000);
	})
}
