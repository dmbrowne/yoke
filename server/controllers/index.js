var fs          = require("fs")
var path        = require("path")
var basename    = path.basename(module.filename)
var Controllers = {}

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== basename)
  })
  .forEach(function(file) {
    var controller = require(path.join(__dirname, file))()
    if( controller.name )
      Controllers[controller.name] = controller;
  });

module.exports = Controllers;
