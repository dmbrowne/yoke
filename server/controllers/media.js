var db       = require('../models')
var $q       = require('q')
var jwt      = require('jsonwebtoken')
var _        = require('lodash')
var cloudinary = require('cloudinary')
var fs = require('fs');

module.exports = function () {
	var MediaController = {

		name: 'Media',
		getAll: getAllMedia,
		getItem: getMediaItem,
		addMedia: addMedia
	}

	return MediaController
}

function getAllMedia(req, res){
	db.Media.findAll({ include: [db.MediaType] })
		.then(function(medi){
			res.send(medi)
		})
		.catch(function(err){
			res.status(400).send(err)
		})
}

function getMediaItem(req, res){
	db.Media.find({
		where: {id: req.params.media_id},
		include: [db.MediaType]
	})
	.then(function(mediaItem){
		res.send(mediaItem)
	})
	.catch(function(err){
		res.status(400).send(err)
	})
}

function addMedia(req, res){
	var files          = req.files || req.body.media
	var userId         = req.body.user_id
	var relationshipId = req.body.relationship_id

	if( _.size(files) === 0 )
		return res.status(400).json({message: 'no files or media array present in request'})

	db.Media.uploadFiles(files, {
		user_id: userId,
		relationship_id: relationshipId
	})
	.then(function(uploadedMediaFiles){
		var addMediaToTable;

		if(_.size(uploadedMediaFiles) === 1 )
			addMediaToTable = db.Media.create(uploadedMediaFiles[0])
		else
			addMediaToTable = db.Media.bulkCreate(uploadedMediaFiles)

		addMediaToTable.then(function(mediaFile){
			var fileCloudIds = _.pluck(uploadedMediaFiles, 'cloud_id')

			if(_.size(mediaFile)) return res.send(mediaFile)

			db.Media.findAll({ where: { cloud_id: fileCloudIds } }).then(function(mediaFiles){
				res.send(mediaFiles)
			})
		})
	})
}
