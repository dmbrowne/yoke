var db       = require('../models')
var env      = process.env.NODE_ENV || "development"
var config   = require(__dirname + '/../config/database_config.json')[env]
var $q       = require('q')
var jwt      = require('jsonwebtoken')
var _        = require('lodash')

module.exports = function(){
	var PostController = {

		name: 'Post',
		getAll: getAll,
		getPost: getPost,
		addPost: addPost,
		updatePost: updatePost
	}

	return PostController
}

function getAll(req, res){
	var relationshipId = req.relationship_id

	db
		.Post
		.findAll({ where: { private: false } })
		.then(function(posts){
			res.send(posts)
		})
}

function getAllPrivatePosts(req, res){
	var relationshipId = req.relationship_id

	db
		.Post
		.findAll({
			where: {
				relationship_id: relationshipId,
				private: true
			}
		})
		.then(function(Posts){
			res.send(Posts)
		})
}

function getPost(req, res){
	var postId = req.params.post_id
	var requestUser = req.user

	db.Post.find(postId)
		.then(function(post){
			res.send(post)
		})
		.catch(function(err){
			console.log(err)
			res.status(500).send()
		})
}

function addPost(req, res){
	var relationshipId = req.params.relationship_id

	db.UserRelationship.checkUserBelongsToRelationship(req.body.user_id, req.body.relationship_id)
		.then(function(exists){
			if(exists){
				db.Post.create(req.body, {
					fields: db.Post.fillable,
				})
				.then(function(newPost){
					res.send(newPost)
				})
			}
			else
				return res.status(403).json({message: 'user_id in request does not belong to the relationship_id from the request'})
		})
		.catch(function(err){
			console.log(err)
			return res.status(500).send()
		})
}

function updatePost(req, res){
	var relationshipId = req.relationship_id
	var userId         = req.user.id

	db.Post.find(req.params.post_id)
		.then(function(postToUpdate){
			if( _.size(postToUpdate) <= 0 )
				return res.status(400).json({ message: 'post id ' + req.params.post_id + 'not found' })

			if(postToUpdate.getDataValue('user_id') !== userId)
				return res.status(401).json({ message: 'token user does not match the owner of post id ' + req.params.post_id })

			postToUpdate.updateAttributes(req.body, { fields: models.Post.updateFillable })
				.then(function(updatedPost){
					res.send(updatedPost)
				})
				.catch(function(err){
					console.log(err)
					res.status(500).send(err)
				})
		})
}
