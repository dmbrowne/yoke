'use strict'

var db       = require('../models')
var env      = process.env.NODE_ENV || "development"
var config   = require(__dirname + '/../config/database_config.json')[env]
var $q       = require('q')
var jwt      = require('jsonwebtoken')
var _        = require('lodash')

module.exports = function () {
	var PrivateConversationController = {

		name: 'PrivateConversation',
		getAll: getAllPrivateConversations,
		getbyRelationshipId: findPrivateConversation,
		getById: findPrivateConversationById,
		create: createPrivateConversationThread
	}

	return PrivateConversationController
}

function getAllPrivateConversations(req, res){
	db.PrivateConversation.findAll({
		include: [db.Relationship]
	})
	.then(function(conversations){
		res.status(200).send(conversations)
	})
	.catch(function(err){
		res.status(250).send(err)
	})
}

function findPrivateConversation(req, res){
	var relationship_id = req.params.relationship_id
	var limit           = req.query.limit || 20

	db.PrivateConversation.find({
		where:   { relationship_id: relationship_id },
		include: [
			{
				model: db.Relationship,
				include: [ db.User ]
			},
			{
				model: db.PrivateMessage,
				limit: limit,
				as: 'Messages',
				order: [
					[ 'created_at', 'DESC' ]
				]
			}
		]
	})
	.then(function(conversation){
		res.status(200).json(conversation)
	})
	.catch(function(err){
		console.log(err)
		res.status(500).send(err)
	})
}

function findPrivateConversationById(req, res){
	var cconvo_id = req.params.id
	var limit     = req.query.limit || 20

	db.PrivateConversation.find({
		where:   { id: cconvo_id },
		include: [{
			model: db.Relationship,
			include: [ db.User ]
		},{
			model: db.PrivateMessage,
			limit: limit,
			as: 'Messages',
			order: [
				[ 'created_at', 'DESC' ]
			]
		}]
	})
	.then(function(conversation){
		res.status(200).json(conversation)
	})
	.catch(function(err){
		console.log(err)
		res.status(500).send(err)
	})
}

function createPrivateConversationThread(req, res){
	var relationshipId = req.params.relationship_id;

	db.PrivateConversation.findOrCreate({
		where: { relationship_id: relationshipId }
	})
	.spread(function(conversation, newlyCreated){
		if(newlyCreated)
			return res.sendStatus(201).json(conversation)
		else
			return res.sendStatus(200).json(conversation)
	})
	.catch(function(err){
		console.log(err)
		return res.sendStatus(400).json(err)
	})
}
