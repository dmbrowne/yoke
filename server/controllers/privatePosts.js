var db       = require('../models')
var env      = process.env.NODE_ENV || "development"
var config   = require(__dirname + '/../config/database_config.json')[env]
var $q       = require('q')
var jwt      = require('jsonwebtoken')
var _        = require('lodash')
var path     = require('path')
var reqStub  = require(path.normalize(__dirname + '/../utils/requestStub'))

module.exports = function(){
	var PrivatePostController = {

		name: 'PrivatePost',
		getAllPrivatePosts: getAllPrivatePosts,
		getPrivatePost: getPost,
		addPrivatePost: addPost,
		updatePrivatePost: updatePost
	}

	return PrivatePostController
}

function getAllPrivatePosts(req, res){
	var relationshipId = req.params.relationship_id
	var requestUser = req.user

	if(!requestUser)
		return res.status(401).json({message: 'token not provided'})

	if( _.size( _.filter(requestUser.Partner, {'UserRelationship': {'relationship_id': relationshipId} }) ) <= 0 )
		return res.status(401).json({message: 'user does not belong to this relationship'})

	db
		.Post
		.findAll({
			where: {
				relationship_id: relationshipId,
				private: true
			},
			include: [db.Media]
		})
		.then(function(posts){
			res.send(posts)
		})
}

function getPost(req, res){
	var postId = req.params.post_id
	var relationshipId = req.params.relationship_id
	var requestUser = req.user

	if(!requestUser)
		return res.status(401).json({message: 'token not provided'})

	if( _.size( _.filter(requestUser.Partner, {'UserRelationship': {'relationship_id': relationshipId} }) ) <= 0 )
		return res.status(401).json({message: 'user does not belong to this relationship'})

	db.Post
		.find({
			where: { id: postId },
			include: [db.Media]
		})
		.then(function(post){

			if(post.getDataValue('user_id') !== requestUser.id)
				return res.status(401).json({ message: 'token user does not match the owner of post id ' + req.params.post_id })

			return res.send(post)
		})
		.catch(function(err){
			console.log(err)
			res.status(500).send()
		})
}

function addPost(req, res){
	var relationshipId = req.params.relationship_id
	var requestUser    = req.user
	var postMedia      = req.files || req.body.media

	if(!requestUser)
		return res.status(401).json({message: 'token not provided'})

	if( _.size( _.filter(requestUser.Partner, {'UserRelationship': {'relationship_id': relationshipId} }) ) <= 0 )
		return res.status(401).json({message: 'user does not belong to this relationship'})

	//set private to true as this route controller is set private posts
	req.body.private = true;

	if(postMedia){
		uploadAndAddMedia().then(function(mediaFile){
			req.body.media_id = mediaFile.getDataValue('id')
			console.log(req.body)
			db.Post.create(req.body, { fields: db.Post.fillable })
				.then(function(newPost){
					return res.send(newPost)
				})
				.catch(function(err){
					console.log(err)
					return res.status(500).send()
				})
		})
	}
	else{
		db.Post.create(req.body, { fields: db.Post.fillable })
			.then(function(newPost){
				return res.send(newPost)
			})
			.catch(function(err){
				console.log(err)
				return res.status(500).send()
			})
	}

	function uploadAndAddMedia(){
		var mediaApiController = require(__dirname + '/media')()
		var reqObj = {
			files: postMedia,
			body: {
				user_id: requestUser.id,
				relationship_id: relationshipId
			}
		}
		return reqStub(reqObj, mediaApiController.addMedia)
	}
}

function updatePost(req, res){
	var relationshipId = req.relationship_id
	var userId         = req.user.id

	db.Post.find(req.params.post_id)
		.then(function(postToUpdate){
			if( _.size(postToUpdate) <= 0 )
				return res.status(400).json({ message: 'post id ' + req.params.post_id + 'not found' })

			if(postToUpdate.getDataValue('user_id') !== userId)
				return res.status(401).json({ message: 'token user does not match the owner of post id ' + req.params.post_id })

			postToUpdate.updateAttributes(req.body, { fields: db.Post.updateFillable })
				.then(function(updatedPost){
					res.send(updatedPost)
				})
				.catch(function(err){
					console.log(err)
					res.status(500).send(err)
				})
		})
}
