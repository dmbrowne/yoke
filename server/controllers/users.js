'use strict';

var db       = require('../models')
var env      = process.env.NODE_ENV || "development"
var config   = require(__dirname + '/../config/database_config.json')[env]
var nodemail = require('../email')
var $q       = require('q')
var chance   = require('chance').Chance(Math.random)
var jwt      = require('jsonwebtoken')
var _        = require('lodash')
var path     = require('path')
var reqStub  = require(path.normalize(__dirname + '/../utils/requestStub'))

module.exports = function () {
	var UserController = {
		name:                'User',
		getAll:              listUsers,
		getCouples:          listCouples,
		getCouple:           getCouple,
		addUser:             addUser,
		getUser:             getUser,
		verifyUser:          userVerify,
		confirmRelationship: confirmUserRelationship,
		login:               getUserAccessToken,
		updateUser:          updateUser,
		updateAvatar:        updateUserAvatar
	}

	return UserController
}

function listUsers(req, res){
	db.User
		.findAll({ include: [db.UserProfile] })
		.then(function(users){ res.send(users) })
		.catch(function(err){
			console.error(err)
			res.status(400).json({message: 'error'})
		})
}

function listCouples(req, res){
	db.Relationship
		.findAll({ include: [db.User] })
		.then(function(users){ res.send(users) })
		.catch(function(err){
			console.error(err)
			res.status(400).json({message: err})
		})
}

function getCouple(req, res){
	db.Relationship
		.find({ where: { id: req.params.couple_id } })
		.then(function(couple){
			res.send(couple)
		})
		.catch(function(err){
			console.error(err)
			res.status(400).json({message: err})
		})
}

function addUser(req, res){
	/*
	* {
	* 	email: foo@bar.com,
	* 	password: happy,
	* 	partner: {
	* 		email: partner@foo.com
	* 	}
	* }
	*/
	if(!req.body.email || !req.body.password)
		return res.status(400).json({ message: 'need both email and password' })

	if(req.body.partner && !req.body.partner.email) return res.status(400).json({ message: 'provide email for partner' })

	var submittedEmail    = req.body.email
	var submittedPassword = req.body.password
	var partnerEmail      = ( req.body.partner && req.body.partner.email ? req.body.partner.email : undefined )
	var partnerPassword   = null
	var checkUser         = [submittedEmail]
	if(partnerEmail) checkUser.push(partnerEmail)

	db.User.find({ where: { email: checkUser } }).then(function(users){
		if(users)
			return res.status(422).json(_.pluck(users, 'email'))

		if(!partnerEmail){
			db.User.usercreate(submittedEmail, submittedPassword, true).then(function(newUser){
				var successResponse = { code: 200, user: newUser }
				return res.status(200).json(successResponse)
			})
		}
		else{
			db.sequelize.transaction().then(function(t){
				return db.User.usercreate(submittedEmail, submittedPassword, true).then(function(newUser){
					var successResponse = { code: 200, user: newUser }
					return db.User.usercreate(partnerEmail, partnerPassword, false).then(function(newPartner){
						successResponse.partner = newPartner
						return db.User.createUserPartnerRelationship(newUser, newPartner).then(function(relationship){
							nodemail.sendPartnerJoinEmail(newUser, newPartner, req)
							successResponse.relationship = relationship
							return successResponse
						})
					})
				})
				.then(function(response){
					t.commit()
					return res.status(200).json(response)
				})
				.catch(function(err){
					t.rollback()
					console.log(err)
					return res.status(500)
				})
			})
		}



	})
}

function getUser(req, res){
	if(req.user)
		getUserByToken(req, res)
	else
		getUserBasic(req, res)
}

function userVerify(req, res){
	var userid = req.params.user_id
	var newPassword = req.body.password
	var verifyToken = req.body.verify_token

	if(!newPassword) return res.status(400).json({ message: "password not recieved" })

	db.User
		.find(userid)
		.then(function(user){
			if ( _.size(user) <= 0 )
				return res.sendStatus(400).json({ message: "user doesn't exist" })

			if( user.getDataValue('verified') !== null )
				return res.status(202).json({message: 'user has already been verified'})

			//Do some logic to check if token has expired
			if( new Date(user.getDataValue('token_expiry')) < new Date() )
				return res.status(400).json({ message: 'token has expired' })

			user.updateAttributes({
				verified: new Date(),
				password: newPassword,
				verify_token: null
			})
			.then(function(updateduser){
				res.status(202).send(updateduser)
			})
			.catch(function(err){
				res.status(400).send(err)
			})
		})
		.catch(function(err){
			console.log(err)
			res.status(500).send(err)
		})
}

function confirmUserRelationship(req, res){
	if( _.size(req.body) < 1 || !req.body.confirm ){
		return res.status(400).json({
				message: 'user relationship confirmation not found in body'})
	}

	db.UserRelationship.find({
		where: {
			relationship_id: req.params.couple_id,
			user_id: req.params.user_id
		}
	})
	.then(function (userRelation){
		userRelation.confirmed = true
		userRelation
			.save()
			.then(function (userRelation){
				res.status(200).json(userRelation)
			})
	})
	.catch(function(err){
		console.log(err)
		res.status(500).send(err)
	})
}

function getUserAccessToken(req, res) {
	var mailOrUser, requestPassword, getUserBy;

	if (req.body.email) {
		mailOrUser = req.body.email;
		getUserBy = 'user';
	}else if (req.body.username) {
		mailOrUser = req.body.username;
		getUserBy = 'profile';
	}else {
		return res.status(401).json({
			message: "provide a username or email to log in with" });
	}

	if (!req.body.password){
		return res.status(401).json({
			message: "provide a password to log in with"
		});
	}

	requestPassword = req.body.password;

	db.User.getUserByUsernameOrProfile(mailOrUser, getUserBy)
		.then(function(user){
			if( user === null )
				return res.status(401).json({ message: "user doesn't exist" });

			user.comparePassword(requestPassword, function(err, isMatch){
				if(err){
					return res.status(500).json({
							message: "error comparing password" });
				}

				if(isMatch){
					var token = jwt.sign(user, config.secret, {
							expiresInMinutes: 60*5 });
					res.json({ user: user, token: token });
				}else{
					return res.status(401).json({ message: "incorrect password" });
				}
			});
		});
}

function getUserByToken(req, res){
	var sessionUser = req.user;

	if(sessionUser === undefined)
		return res.status(401).json({message: "access token not recieved"});

	if(sessionUser.id !== req.params.user_id){
		return res.status(401).json({
				message: "access token and user id do not match"});
	}

	db.User
		.find({ where: { id: sessionUser.id },
			include: [{
				model: db.UserProfile,
				include: {
					model: db.Media,
					as: 'Avatar'
				}
			},
			{
				model: db.User,
				as: 'Partner',
				include: [{
					model: db.UserProfile,
					include: {
						model: db.Media,
						as: 'Avatar'
					}
				}]
			}]
		})
		.then(function(user){
			return res.send(user);
		})
		.catch(function(err){
			console.error(err);
			return res.status(500).send(err);
		})
}

function getUserBasic(req, res){
	var userid = req.params.user_id

	db.User
		.find(userid)
		.then(function(user){
			user.removeSensitiveValues()
			res.send(user)
		})
		.catch(function(err){
			console.log(err)
			res.status(400).send(err)
		})
}

function updateUser(req, res){
	var userId = req.user.id, updateOperations  = []

	var userupdates = db.sequelize
		.transaction()
		.then(function (t){
			var deferred = $q.defer()

			if( req.body.email || req.body.password )
				updateOperations.push(updateUserModel(t))

			if( req.body.profile )
				updateOperations.push(updateUserProfile(t))

			$q
				.all(updateOperations)
				.then(function(results) {
					t.commit()
					deferred.resolve()
				})
				.catch(function(err){
					t.rollback()
					deferred.reject(err)
				})

			return deferred.promise
		})

	userupdates.then(function (){
		reqStub(req, getUserByToken)
			.then(function (user){
				return res.send(user);
			})
			.catch(function (err){
				console.error(err);
				return res.sendStatus(500).json(err);
			})
	})

	function updateUserProfile(transAction) {
		var transactionStatement = transAction ?
				{ transaction: transAction } :
				undefined;
		var profile     = req.body.profile
		// var allowedVals = db.UserProfile.fillable
		// profile.user_id = userId
		// allowedVals.push('user_id')

		return db.UserProfile
			.findOrCreate({where: {user_id: userId} }, transactionStatement)
			.spread(function (usrProfile, newlyCreated) {
				return usrProfile.update(profile, transactionStatement)
			})
	}

	function updateUserModel(transAction) {
		var transactionStatement = transAction ?
				{ transaction: transAction } :
				undefined;
		return db.User.update(req.body, {
			where: { id: userId },
			fields: db.User.fillable
		}, transactionStatement)
	}
}

function updateUserAvatar(req, res){
	var files  = req.files || req.body.avatar
	var userId = req.user.id

	if( _.size(files) === 0 ){
		return res.status(400).json({
				message: 'no files or media array present in request'})
	}

	db.Media
		.uploadFiles(files, {
			user_id: userId
		})
		.then(function(mediaObjsToSave){
			db.sequelize
				.transaction(function(t){
					return db.Media
						.create(mediaObjsToSave[0], { transaction: t })
						.then(function (savedMediaItem) {
							return db.UserProfile
								.findOrCreate({ where: { user_id: userId }}, {
										transaction: t })
								.spread(function (userProfile, isNew) {
									return userProfile.setAvatar(savedMediaItem, { transaction: t })
								})
						})
				})
				.then(function(){
					db.UserProfile
						.find({ where: { user_id: userId } })
						.then(function(profile){
							res.send(profile)
						})
				})
				.catch(function(err){
					res.status(500).json(err)
				})
		})
}
