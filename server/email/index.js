'use strict'

var env              = process.env.NODE_ENV || "development"
var config           = require('../config/mailgun_config')
var nodemailer       = require('nodemailer')
var mailgunTransport = require('nodemailer-mailgun-transport')
var _                = require('lodash')
var $q               = require('q')
var env              = process.env.NODE_ENV || "development";

// This is your API key that you retrieve from www.mailgun.com/cp (free up to 10K monthly emails)
var auth = {
	auth: {
		api_key: config[env].mailgun.apikey,
		domain: config[env].mailgun.domain
	}
}

var nodemailerMailgun = nodemailer.createTransport(mailgunTransport(auth));

module.exports = {
	sendEmail:            sendEmail,
	sendPartnerJoinEmail: sendPartnerJoinEmail
}

function sendEmail(options){
	var deferred = $q.defer()

	if( _.size(options) <= 0 )
		deferred.reject('no options set')

	if( !options.to || !options.from || (!options.html && !options.text) )
		deferred.reject('need correct options')

	var optionsKeys = ['to', 'from', 'subject', 'html', 'text']
	var mailConfig  = {}

	_.forEach(optionsKeys, function(key){
		if ( options[key] )
			mailConfig[key] = options[key]
	})

	nodemailerMailgun.sendMail(
		mailConfig,
		function (err, info){
			if(err)
				deferred.reject(err)
			else
				deferred.resolve(info)
		})

	return deferred.promise
}


function sendPartnerJoinEmail(userInstnace, partnerInstance, request){
	var domain       = "http://"+request.get('host')
	var verifyRoute  = "/api/v1/verifyuser"
	var reqParams    = "?user_id="+partnerInstance.getDataValue('id')+"&verify_token="+partnerInstance.getDataValue('verify_token')
	var partnerEmail = partnerInstance.getDataValue('email')
	// var verifyUrl    = domain + verifyRoute + reqParams;
	var verifyUrl    = "http://localhost:3000/verifyaccount/" + reqParams
	// var verifyUrl    = "http://ouryoke.com/verifyaccount/" + reqParams

	if(env === 'test'){
		return true
	} else {
		return sendEmail({
			from: 'daryl.browne@btinternet.com',
			to: partnerEmail,
			subject: 'Confirm your yokE&reg; account',
			html: 'An account has been created for you by your partner: ' + userInstnace.getDataValue('email') + '</b> \n please visit the url below to confirm your account and relationship: \n' + verifyUrl,
			text: 'Mailgun rocks, pow pow!'
		})
	}
}
