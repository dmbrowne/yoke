"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.sequelize.query(
			"CREATE OR REPLACE FUNCTION mongodb_object_id(OUT result varchar) AS $$ " +
			    "DECLARE " +
			        "time_component bigint; " +
			        "machine_id int := FLOOR(random() * 16777215); " +
			        "process_id int; " +
			        "seq_id bigint := FLOOR(random() * 16777215); " +
			    "BEGIN " +
			        "SELECT FLOOR(EXTRACT(EPOCH FROM clock_timestamp())) INTO time_component; " +
			        "SELECT pg_backend_pid() INTO process_id; " +

			        "result := lpad(to_hex(time_component), 8, '0'); " +
			        "result := result || lpad(to_hex(machine_id), 6, '0'); " +
			        "result := result || lpad(to_hex(process_id), 4, '0'); " +
			        "result := result || lpad(to_hex(seq_id), 6, '0'); " +
			    "END; " +
			"$$ LANGUAGE PLPGSQL;"
		).then(function(){
			done();
		})
	},

	down: function(migration, DataTypes, done) {
		return migration.dropTable('DROP FUNCTION mongodb_object_id()').then(function(){ done() })
	}
};
