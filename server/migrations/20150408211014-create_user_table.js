"use strict";

module.exports = {
	up: function(migration, DataTypes) {
		return migration.createTable('users', {
			id:{
				type: DataTypes.STRING(24),
				primaryKey: true,
				allowNull: false,
				defaultValue: migration.sequelize.fn('mongodb_object_id')
			},

			email: {
				type: DataTypes.STRING(50),
				allowNull: false,
				unique: true
			},

			password: {
				type: DataTypes.STRING,
				allowNull: true
			},

			verified: {
				type: DataTypes.DATE,
				allowNull: true,
			},

			verify_token: {
				type: DataTypes.STRING,
      			allowNull: true
			},

			token_expiry:{
				type: DataTypes.DATE,
				allowNull: true
			},

			created_at: DataTypes.DATE,

			updated_at: {
				type: DataTypes.DATE,
				defaultValue: DataTypes.NOW
			},

			deleted_at: DataTypes.DATE
		})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('users')
	}
};
