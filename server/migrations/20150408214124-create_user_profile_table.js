"use strict";

module.exports = {

	up: function(migration, DataTypes) {
		return migration.createTable('user_profiles', {

		id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			primaryKey: true,
			defaultValue: migration.sequelize.fn('mongodb_object_id')
		},

		user_id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			references: "users",
			referencesKey: "id",
			unique: true
		},

		first_name: DataTypes.STRING(15),

		last_name: DataTypes.STRING(15),

		bio: DataTypes.TEXT,

		gender:{
			type: DataTypes.ENUM,
			values: ['male', 'female', 'other'],
			allowNull: false,
			defaultValue: 'other'
		},

		created_at: DataTypes.DATE,

		updated_at: {
			type: DataTypes.DATE,
			defaultValue: DataTypes.NOW
		},

		deleted_at: DataTypes.DATE
	})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('user_profiles')
	}
};
