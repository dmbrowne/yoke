"use strict";

module.exports = {
	up: function(migration, DataTypes, done) {
		migration.createTable('relationships', {

			id: {
				type: DataTypes.STRING(24),
				allowNull: false,
				primaryKey: true,
				defaultValue: migration.sequelize.fn('mongodb_object_id')
			},

			name: DataTypes.STRING(50),

			bio: DataTypes.TEXT,

			created_at: DataTypes.DATE,

			updated_at: {
				type: DataTypes.DATE,
				defaultValue: DataTypes.NOW
			},

			deleted_at: DataTypes.DATE
		})
		.then(function(){ done() })
	},

	down: function(migration, DataTypes, done) {
		migration.sequelize.query('drop table relationships CASCADE')
		// migration.dropTable('relationships', { onDelete: 'CASCADE'})
		.then(function(){ done() })
	}
};
