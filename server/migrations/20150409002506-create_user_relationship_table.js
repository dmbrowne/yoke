"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('user_relationships', {
    	relationship_id: {
				type: DataTypes.STRING(24),
				references: 'relationships',
				referencesKey: 'id'
			},

			user_id: {
				type: DataTypes.STRING(24),
				references: 'users',
				referencesKey: 'id'
			},

      partner_id: {
        type: DataTypes.STRING(24),
        references: 'users',
        referencesKey: 'id'
      },

      confirmed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },

      created_at: DataTypes.DATE,

      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },

      deleted_at: DataTypes.DATE
    })
    .then(function(){
    	return migration.addIndex('user_relationships', ['user_id', 'partner_id'], {
					indicesType: 'UNIQUE',
					indexName: 'uniqueUserRelationship'
				})
    })
  },

  down: function(migration, DataTypes) {
    return migration.removeIndex('user_relationships', 'uniqueUserRelationship')
      .then(function(){
        return migration.dropTable('user_relationships')
      })
  }
};
