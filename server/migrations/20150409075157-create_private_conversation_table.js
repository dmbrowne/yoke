"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('private_conversations', {
      id: {
        type: DataTypes.STRING(24),
        primaryKey: true,
        allowNull: false,
        defaultValue: migration.sequelize.fn('mongodb_object_id')
      },

      relationship_id: {
  		  type: DataTypes.STRING(24),
  		  allowNull: false,
  		  unique: true,
  		  references: 'relationships',
  		  referencesKey: 'id'
  		},

      created_at: DataTypes.DATE,

      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },

      deleted_at: DataTypes.DATE
    })
  },

  down: function(migration, DataTypes) {
    return migration.dropTable('private_conversations')
  }
};
