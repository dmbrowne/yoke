'use strict'

module.exports = {
  up: function (migration, DataTypes) {
    return migration.createTable('private_messages', {
      id: {
        type: DataTypes.STRING(24),
        primaryKey: true,
        allowNull: false,
        defaultValue: migration.sequelize.fn('mongodb_object_id')
      },

      sender_id: {
        type: DataTypes.STRING(24),
        allowNull: false,
        references: 'users',
        referencesKey: 'id'
      },

      receipient_id: {
        type: DataTypes.STRING(24),
        allowNull: false,
        references: 'users',
        referencesKey: 'id'
      },

      conversation_id: {
        type: DataTypes.STRING(24),
        allowNull: false,
        references: 'private_conversations',
        referencesKey: 'id'
      },

      message: DataTypes.TEXT,

      created_at: DataTypes.DATE,

      updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
      },

      deleted_at: DataTypes.DATE
    })
  },

  down: function (migration, DataTypes) {
    return migration.dropTable('private_messages')
  }
}
