"use strict";

module.exports = {
	up: function(migration, DataTypes) {
		return migration.createTable('item_types', {
			id:{
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},

			type: {
				type: DataTypes.STRING(30),
				allowNull: false
			}
		})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('item_types')
	}
};
