"use strict";

module.exports = {
	up: function(migration, DataTypes) {
		return migration.createTable('items', {
			id:{
				type: DataTypes.STRING(24),
				primaryKey: true,
				allowNull: false,
				defaultValue: migration.sequelize.fn('mongodb_object_id')
			},

			itemtype_id: {
				type: DataTypes.INTEGER,
				references: 'item_types',
				referencesKey: 'id'
			},

			created_at: DataTypes.DATE,

			updated_at: {
				type: DataTypes.DATE,
				defaultValue: DataTypes.NOW
			},

			deleted_at: DataTypes.DATE
		})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('items')
	}
};
