"use strict";

module.exports = {
	up: function(migration, DataTypes) {
		return migration.createTable('media_types', {

			id: {
				type: DataTypes.INTEGER,
				autoIncrement: true,
				primaryKey: true
			},

			type: {
				type: DataTypes.STRING(10),
				allowNull: false,
				unique: true
			}

		})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('media_types')
	}
};
