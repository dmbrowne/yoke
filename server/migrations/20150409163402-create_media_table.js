"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('media', {

    	id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			primaryKey: true,
			defaultValue: migration.sequelize.fn('mongodb_object_id')
		},

    	cloud_id:{
			allowNull: false,
			type: DataTypes.STRING
		},

		item_id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			unique: true,
			references: 'items',
			referencesKey: 'id'
		},

		media_type_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: 'media_types',
			referencesKey: 'id'
		},


		user_id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			references: 'users',
			referencesKey: 'id'
		},

		relationship_id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			references: 'relationships',
			referencesKey: 'id'
		},

		created_at: DataTypes.DATE,

		deleted_at: DataTypes.DATE

    })
  },

  down: function(migration, DataTypes) {
     return migration.dropTable('media')
  }
};
