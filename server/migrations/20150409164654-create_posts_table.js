"use strict";

module.exports = {
	up: function(migration, DataTypes) {
		return migration.createTable('posts', {
			id: {
				type: DataTypes.STRING(24),
				primaryKey: true,
				allowNull: false,
				defaultValue: migration.sequelize.fn('mongodb_object_id')
			},

			item_id: {
				type: DataTypes.STRING(24),
				allowNull: false,
				unique: true,
				references: 'items',
				referencesKey: 'id'
			},

			post_type: {
				type: DataTypes.STRING(15),
				allowNull: false,
				defaultValue: 'standard'
			},

			title: {
				type: DataTypes.STRING(50),
				defaultValue: null
			},

			content: {
				type: DataTypes.TEXT,
				defaultValue: null
			},

			secret: {
				type: DataTypes.BOOLEAN,
				defaultValue: false
			},

			self_destruct: {
				type: DataTypes.INTEGER(3),
				defaultValue: 0
			},

			private: {
			    type: DataTypes.BOOLEAN,
			    defaultValue: true,
			    allowNull: false
			},

			relationship_id:{
				type: DataTypes.STRING(24),
				allowNull: false,
				references: 'relationships',
				referencesKey: 'id'
			},

			media_id:{
				type: DataTypes.STRING(24),
				allowNull: true,
				references: 'media',
				referencesKey: 'id'
			},

			user_id:{
				type: DataTypes.STRING(24),
				allowNull: false,
				references: 'users',
				referencesKey: 'id'
			},

			created_at: DataTypes.DATE,

			updated_at: {
				type: DataTypes.DATE,
				defaultValue: DataTypes.NOW
			},

			deleted_at: DataTypes.DATE
		})
	},

	down: function(migration, DataTypes) {
		return migration.dropTable('posts')
	}
};
