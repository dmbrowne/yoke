"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('notification_subjects', {
    	id: {
    		primaryKey: true,
    		autoIncrement: true,
    		type: DataTypes.INTEGER
    	},

    	subject: {
				type: DataTypes.STRING(30),
				allowNull: false
			}

    })
  },

  down: function(migration, DataTypes) {
    return migration.dropTable('notification_subjects')
  }
};
