"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('notification_verbs', {
    	id: {
    		primaryKey: true,
    		autoIncrement: true,
    		type: DataTypes.INTEGER
    	},

    	action: {
				type: DataTypes.STRING(30),
				allowNull: false
			}

    })
  },

  down: function(migration, DataTypes) {
    return migration.dropTable('notification_verbs')
  }
};
