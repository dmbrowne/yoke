"use strict";

module.exports = {
  up: function(migration, DataTypes) {
    return migration.createTable('private_notifications', {
    	id: {
    		allowNull: false,
    		primaryKey: true,
    		type: DataTypes.STRING(24),
    		defaultValue: migration.sequelize.fn('mongodb_object_id')
    	},

    	user_id: {
			allowNull: false,
			references: 'users',
			referencesKey: 'id',
			type: DataTypes.STRING(24)
		},

		relationship_id: {
			allowNull: false,
			references: 'relationships',
			referencesKey: 'id',
			type: DataTypes.STRING(24)
		},

		subject_id: {
			allowNull: false,
			references: 'notification_subjects',
			referencesKey: 'id',
			type: DataTypes.INTEGER
		},

		verb_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			references: 'notification_verbs',
			referencesKey: 'id'
		},

		is_read: {
			type: DataTypes.BOOLEAN,
			defaultValue: false,
			allowNull: false
		},

		created_at: DataTypes.DATE,

		updated_at: {
			type: DataTypes.DATE,
			defaultValue: DataTypes.NOW
		},

		deleted_at: DataTypes.DATE
    })
  },

  down: function(migration, DataTypes) {
    return migration.dropTable('private_notifications')
  }
};
