'use strict';

var model = require('../models');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return model.ItemType.bulkCreate([
      { type: 'post' },
      { type: 'media'},
      { type: 'collection'}
    ])
  },

  down: function (queryInterface, Sequelize) {
    return model.ItemType.destroy({
      where: {
        type: ['post', 'media', 'collection']
      }
    })
  }
};
