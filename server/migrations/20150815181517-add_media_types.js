'use strict';

var model = require('../models');

module.exports = {
  up: function (queryInterface, Sequelize) {
    return model.MediaType.bulkCreate([
      { type: 'video' },
      { type: 'image'}
    ])
  },

  down: function (queryInterface, Sequelize) {
    return model.MediaType.destroy({
      where: {
        type: ['image', 'video']
      }
    })
  }
};
