'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn('media', 'relationship_id', {
        type: Sequelize.STRING(24),
        allowNull: true
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.changeColumn('media', 'relationship_id', {
        type: Sequelize.STRING(24),
        allowNull: false
    })
  }
};
