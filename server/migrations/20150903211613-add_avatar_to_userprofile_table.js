'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('user_profiles', 'avatar_media_id', {
        type:          Sequelize.STRING(24),
        allowNull:     true,
        references:    'media',
        referencesKey: 'id'
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('user_profiles', 'avatar_media_id')
  }
};
