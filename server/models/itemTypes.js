'use strict'

module.exports = function(sequelize, DataTypes){
	var ItemType = sequelize.define("ItemType", {
		id: {
			primaryKey: true,
			autoIncrement: true,
			type: DataTypes.INTEGER
		},

		type: {
			type: DataTypes.STRING(10),
			allowNull: false,
			unique: true
		}
	},
	{
		paranoid: false,
		timestamps: false,
		tableName: 'item_types',
		classMethods: {
			associate: function(models){
				ItemType.hasMany(models.Item)
			}
		}
	})

	return ItemType
}
