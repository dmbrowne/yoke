'use strict'

module.exports = function(sequelize, DataTypes){
	var Item = sequelize.define("Item", {

		itemtype_id: {
			type: DataTypes.INTEGER,
			references: 'item_types',
			referencesKey: 'id'
		}

	},
	{
		paranoid: false,
		timestamps: true,
		tableName: 'items',
		classMethods: {
			associate: function(models){
				Item.belongsTo(models.ItemType)
			}
		}
	})

	return Item
}
