'use strict'
var rootDir = __dirname + '/../../';
var _ = require('lodash')
var $q = require('q')
var cloudinary = require('cloudinary')
var fs = require('fs')
var path = require('path')

module.exports = function(sequelize, DataTypes){
	var Media = sequelize.define('Media', {
		cloud_id:{
			allowNull: false,
			type: DataTypes.STRING
		},

		item_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		media_type_id: {
			type: DataTypes.INTEGER,
			allowNull: false
		},

		user_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		relationship_id: {
			type: DataTypes.STRING(24),
			allowNull: true
		}
	},
	{
		getterMethods: {
			thumbnail: function(){
				return cloudinary.url(this.cloud_id, { width: 150, height: 150, crop: 'fill', quality: 30 } )
			},

			medium: function(){
				return cloudinary.url(this.cloud_id, { width: 500, height: 500, crop: 'limit', quality: 50 } )
			},

			high: function(){
				return cloudinary.url(this.cloud_id, {quality: 60} )
			},

			full: function(){
				return cloudinary.url(this.cloud_id )
			}
		},
		timestamps: true,
		updatedAt: false,
		tableName: 'media',
		freezeTableName: true,
		classMethods: {
			uploadFiles: uploadFiles,
			mediaInsertWithMediatypeConversion: mediaInsertWithMediatypeConversion,
			associate: function(models){
				Media.belongsTo(models.MediaType, { foreignKey: 'media_type_id' })
				Media.hasOne(models.Post, {foreignKey: 'media_id'})
				Media.belongsTo(models.User)
				Media.belongsTo(models.Relationship)
				Media.belongsTo(models.Item)
			}
		}
	})

	return Media;

	function uploadFiles(filesToUpload, userDetails){
		var buildArray  = []
		var allPromises = []
		var deferred    = $q.defer()

		// ### TO DO ###
		// -------------
		// Sanity check that userDetails are strings


		_.forEach(filesToUpload, function(file){
			var fileRepresentation, fileUploadPromise, filePath

			if (file.path)
				fileRepresentation = 'fileObject'
			else
				fileRepresentation = 'base64'

			//fileObject or base64 string
			filePath = file.path || file

			/* create a new promise on each iteration so that they can call individually resolved
			 * return the function once all iteractions promises are resolved */
			fileUploadPromise = $q.defer();
			allPromises.push( fileUploadPromise.promise )

			cloudinary.uploader.upload(filePath, function(result) {
				var fileToDelete = (fileRepresentation == 'fileObject' ?
						file.path :
						undefined )

				buildMediaObjectAndRemoveFile(result, fileToDelete)
					.then(function(objectToBuild){
						buildArray.push(objectToBuild)
						fileUploadPromise.resolve()
					})
			});
		})

		$q.all(allPromises).then(function(){
			deferred.resolve(buildArray)
		})

		return deferred.promise

		function buildMediaObjectAndRemoveFile(result, fileToDelete) {
			/*
			* // Sample Result
			* {
			*   public_id: 'cr4mxeqx5zb8rlakpfkg',
			*   version: 1372275963,
			*   signature: '63bfbca643baa9c86b7d2921d776628ac83a1b6e',
			*   width: 864,
			*   height: 576,
			*   format: 'jpg',
			*   resource_type: 'image',
			*   created_at: '2013-06-26T19:46:03Z',
			*   bytes: 120253,
			*   type: 'upload',
			*   url: 'http://res.cloudinary.com/demo/image/upload/v1372275963/cr4mxeqx5zb8rlakpfkg.jpg',
			*   secure_url: 'https://res.cloudinary.com/demo/image/upload/v1372275963/cr4mxeqx5zb8rlakpfkg.jpg'
			* }
			*/
			var deferred                = $q.defer()
			var mediaType, mediaTypeId, itemTypeId, dbSaveData, createNewItem
			var buildMediaObject        = buildMediaObjectFn
			var removeFileFromUploadDir = removeFileFromUploadDirFn
			var typeIdPromises          = []

			if(result.resource_type === 'image') mediaType = 'image' // 1
			if(result.resource_type === 'video') mediaType = 'video' // 2

			typeIdPromises.push(
					sequelize.model('MediaType')
						.find({ where: { type: mediaType } }) )

			typeIdPromises.push(
					sequelize.model('ItemType')
						.find({ where: { type: 'media' } }) )

			createNewItem = $q.defer()

			$q.all(typeIdPromises)
				.spread(function(mediatypes, itemtype){
					mediaTypeId = mediatypes.dataValues.id
					itemTypeId  = itemtype.dataValues.id

					sequelize.model('Item')
						.create({ itemtype_id: itemTypeId })
						.then(function(newItemModel){
							createNewItem.resolve(newItemModel)
						})
				})

			createNewItem
				.promise
				.then(function (newItem) {
					if (fileToDelete)
						removeFileFromUploadDir(fileToDelete)

					deferred.resolve( buildMediaObject(newItem) )
				})

			return deferred.promise


			function buildMediaObjectFn(item) {
				dbSaveData = {
					item_id:       item.getDataValue('id'),
					cloud_id:      result.public_id,
					media_type_id: mediaTypeId,
					user_id:       userDetails.user_id
				}

				if(userDetails.relationship_id)
					dbSaveData.relationship_id = userDetails.relationship_id

				return dbSaveData
			}

			function removeFileFromUploadDirFn(file)   {
				fs.unlink( path.normalize(__dirname + '/../../' + file),
					function (err) {
						if (err) throw new Error(err);
					})
			}
		}
	}

	function mediaInsertWithMediatypeConversion(mediaBuildObjs){
		return sequelize.transaction(function(t){
			return sequelize.Model('MediaType').findAll({
				transaction: t,
				raw: true
			})
			.then(function(mediatypes){
				return sequelize.Promise.map(mediaBuildObjs, function(buildObj){
					var mediaTypeId = _.chain(
							mediatypes
						)
						.filter({ name: buildObj.media_type })
						.pluck('id')

					buildObj.media_type_id = mediaTypeId
					delete buildObj.media_type
					return buildObj
				})
				.then(function(convertedBuildObjs){
					return Media.bulkCreate(convertedBuildObjs, { transaction: t })
				})
			})
		})
	}
}
