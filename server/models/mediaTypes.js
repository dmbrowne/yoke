'use strict'

module.exports = function(sequelize, DataTypes){
	var MediaType = sequelize.define("MediaType", {
		id: {
			primaryKey: true,
			autoIncrement: true,
			type: DataTypes.INTEGER
		},

		type: {
			type: DataTypes.STRING(10),
			allowNull: false,
			unique: true
		}
	},
	{
		paranoid: false,
		timestamps: false,
		tableName: 'media_types',
		classMethods: {
			associate: function(models){
				MediaType.hasMany(models.Media)
			}
		}
	})

	return MediaType
}
