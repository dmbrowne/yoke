'use strict'

module.exports = function (sequelize, DataTypes) {
	var NotificationSubject = sequelize.define('NotificationSubject', {

		id: {
			primaryKey: true,
			type: DataTypes.INTEGER(2),
			autoIncrement: true
		},

		event_name: {
			type: DataTypes.STRING(40),
			allowNull: false
		}

	},
	{
		timestamps: false,
		tableName: 'notification_subjects',
		classMethods: {
			associate: function(models) {
				// NotificationSubject.hasMany(models.PrivateNotification)
			}
		}
	})

	return NotificationSubject
}
