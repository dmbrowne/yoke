'use strict'

module.exports = function (sequelize, DataTypes) {
	var NotificationVerb = sequelize.define('NotificationVerb', {

		id: {
			primaryKey: true,
			type: DataTypes.INTEGER(2),
			autoIncrement: true
		},

		verb: {
			type: DataTypes.STRING(40),
			allowNull: false
		}

	},
	{
		timestamps: false,
		tableName: 'notification_verbs',
		classMethods: {
			associate: function(models) {
				// NotificationVerb.hasMany(models.RelationshipNotification)
				// NotificationVerb.hasMany(models.PrivateNotification)
			}
		}
	})

	return NotificationVerb
}
