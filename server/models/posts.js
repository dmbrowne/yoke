'use strict'

module.exports = function(sequelize, DataTypes){
	var Post = sequelize.define("Post", {

		post_type: {
			type: DataTypes.STRING(15),
			allowNull: false,
			defaultValue: 'standard'
		},

		item_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		title: {
			type: DataTypes.STRING(50),
			defaultValue: null
		},

		content: {
			type: DataTypes.TEXT,
			defaultValue: null
		},

		// require password to open
		secret: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		},

		// Timer for snapchat like photo
		self_destruct: {
			type: DataTypes.INTEGER(3),
			defaultValue: 0,
			validate: {
				min: 0,
				max: 180
			}
		},

		// public can view - or just partner?
		private: {
			type: DataTypes.BOOLEAN,
			defaultValue: true
		},

		relationship_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		},

		media_id:{
			type: DataTypes.STRING(24),
			allowNull: true
		},

		user_id:{
			type: DataTypes.STRING(24),
			allowNull: false
		}
	},
	{
		timestamps: true,
		tableName: 'posts',

		classMethods:{
			fillable: ['title', 'content', 'secret', 'self_destruct', 'relationship_id', 'user_id', 'media_id'],
			updateFillable: ['title', 'content', 'self_destruct'],
			allowedPostTyes: ['standard', 'note'],
			associate: function(models){
				Post.belongsTo(models.User)
				Post.belongsTo(models.Media, {foreignKey: 'media_id'})
				Post.belongsTo(models.Relationship, { foreignKey: 'relationship_id'})
				Post.belongsTo(models.Item)
			}
		},

		validate:{
			titleOrContent: function() {
				if( (this.title === null && this.content === null) || (this.title === '' && this.content === ''))
					throw new Error('Either the "title" or "content", or both need to be set')
			},

			postType: function(){
				if( Post.allowedPostTyes.indexOf(this.post_type) < 0 )
					throw new Error('this post type is not allowed')
			}
		}
	})

	return Post
}
