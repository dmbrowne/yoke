'use strict'

module.exports = function(sequelize, DataTypes){
	var PrivateConversation = sequelize.define('PrivateConversation', {

		relationship_id: {
			type: DataTypes.STRING(24),
			allowNull: false,
			unique: true
		}
	},
	{
		timestamps:   true,
		tableName:    'private_conversations',
		classMethods: {
			associate: function(models) {
				PrivateConversation.belongsTo(models.Relationship, { foreignKey: 'relationship_id' })
				PrivateConversation.hasMany(models.PrivateMessage, { foreignKey: 'conversation_id', as: 'Messages' })
			}
		}

	})
	return PrivateConversation
}
