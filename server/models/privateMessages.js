'use strict'

module.exports = function(sequelize, DataTypes){
	var PrivateMessage = sequelize.define('PrivateMessage', {

		sender_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		receipient_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		conversation_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		message: DataTypes.TEXT

	},
	{
		timestamps:   true,
		tableName:    'private_messages',
		classMethods: {
			associate: function(models) {
				PrivateMessage.belongsTo(models.User, { foreignKey: 'sender_id' })
				PrivateMessage.belongsTo(models.User, { foreignKey: 'receipient_id' })
				PrivateMessage.belongsTo(models.PrivateConversation, { foreignKey: 'conversation_id', as: 'Messages' })
			}
		}

	})
	return PrivateMessage
}
