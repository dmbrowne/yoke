'use strict'

module.exports = function(sequelize, DataTypes){
	var PrivateNotification = sequelize.define('PrivateNotification', {
		// user_id: {
		// 	type: DataTypes.STRING(24),
		// 	allowNull: false
		// },

		// relationship_id: {
		// 	type: DataTypes.STRING(24),
		// 	allowNull: false
		// },

		// subject_id: {
		// 	type: DataTypes.INTEGER,
		// 	allowNull: false
		// },

		// verb_id: {
		// 	type: DataTypes.INTEGER,
		// 	allowNull: false
		// },

		// is_read: {
		// 	type: DataTypes.BOOLEAN,
		// 	defaultValue: false,
		// 	allowNull: false
		// }
	},
	{
		timestamps: true,
		updatedAt: false,
		tablename: 'private_notifications',
		classMethods: {
			associate: function(models){
				// PrivateNotification.belongsTo(models.User)
				// PrivateNotification.belongsTo(models.Relationship, {foreignKey: 'relationship_id'})
				// PrivateNotification.belongsTo(models.NotificationSubject, {foreignKey: 'subject_id'})
				// PrivateNotification.belongsTo(models.NotificationVerb, {foreignKey: 'verb_id'})
			}
		}
	})

	return PrivateNotification
}
