"use strict"

module.exports = function(sequelize, DataTypes) {
	var Relationship = sequelize.define("Relationship", {

		name: DataTypes.STRING(50),

		bio: DataTypes.TEXT
	},
	{
		timestamps:    true,
		tableName:     'relationships',
		classMethods:  {
			associate: function(models) {
				Relationship.hasOne(models.PrivateConversation, { foreignKey: 'relationship_id' })

				// Relationship.hasMany(models.PrivateNotification)

				// Relationship.hasMany(models.RelationshipNotification, { foreignKey: 'sender_id'})
				// Relationship.hasMany(models.RelationshipNotification, { foreignKey: 'reciever_id'})

				Relationship.hasMany(models.Media)

				Relationship.hasMany(models.Post)


				Relationship.belongsToMany(models.User, {through: models.UserRelationship, foreignKey: 'relationship_id'})
				Relationship.belongsToMany(models.User, {through: models.UserRelationship, foreignKey: 'relationship_id', as: 'Partner'})
			}
		}
	})

	return Relationship;
}

