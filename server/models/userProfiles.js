"use strict"

module.exports = function(sequelize, DataTypes) {
	var UserProfile = sequelize.define("UserProfile", {

		user_id: {
			type: DataTypes.STRING(24),
			allowNull: false
		},

		avatar_media_id: {
			type: DataTypes.STRING(24),
			allowNull: true
		},

		first_name: DataTypes.STRING(15),

		last_name: DataTypes.STRING(15),

		bio: DataTypes.TEXT,

		gender:{
			allowNull: false,
			type: DataTypes.ENUM,
			defaultValue: 'other',
			values: ['male', 'female', 'other']
		}
	},
	{
		timestamps:      true,
		underscored:     true,
		tableName:       'user_profiles',
		classMethods:    {
			fillable: ['first_name', 'last_name', 'bio', 'gender'],
			associate: function(models) {
				UserProfile.belongsTo(models.User);
				UserProfile.belongsTo(models.Media, { foreignKey: 'avatar_media_id', as: 'Avatar' });
			}
		}
	})

	return UserProfile;
}
