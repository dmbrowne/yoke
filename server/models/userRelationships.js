"use strict"

var _ = require('lodash')
var $q = require('q')


module.exports = function(sequelize, DataTypes) {
	var UserRelationship = sequelize.define("UserRelationship", {

		relationship_id: DataTypes.STRING(24),

		user_id: {
			type: DataTypes.STRING(24),
			unique: 'uniqueUserRelationship'
		},

		partner_id: {
			type: DataTypes.STRING(24),
			unique: 'uniqueUserRelationship'
		},

		confirmed: {
			type: DataTypes.BOOLEAN,
			defaultValue: false
		}
	},
	{
		timestamps: true,
		tableName:  'user_relationships',
		validate:{
			differentPartnerIds: function() {
				if( this.user_id === this.partner_id )
					throw new Error('"user_id" and "partner_id" must not be the same')
			}
		},
		classMethods: {
			checkUserBelongsToRelationship: checkUserBelongsToRelationship
		}
	})

	return UserRelationship;

	function checkUserBelongsToRelationship(userId, relationshipId){
		var deferred = $q.defer()

		if(!userId || !relationshipId) deferred.reject('method needs both userId and relationshipId')

		sequelize.model('UserRelationship').find({ where: {
			user_id: userId,
			relationship_id: relationshipId
		}})
		.then(function(userrelationship){
			if( _.size(userrelationship) <= 0 )
				deferred.resolve(false)
			else
				deferred.resolve(true)
		})
		.catch(function(err){
			deferred.reject(err)
		})

		return deferred.promise
	}
}

