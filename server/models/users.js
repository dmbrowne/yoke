"use strict"

var bcrypt = require('bcrypt');
var $q     = require('q');
var jwt    = require('jsonwebtoken');
var env    = process.env.NODE_ENV || "development";
var config = require(__dirname + '/../config/database_config.json')[env];
var chance = require('chance').Chance();

var User, sequelize;

module.exports = function(Sequelize, DataTypes) {
	var userSchema = schema(DataTypes)
	sequelize      = Sequelize
	User           = sequelize.define("User", userSchema, {
		timestamps:      true,
		tableName:       'users',
		classMethods:    {
			fillable:                      ['email', 'password'],
			associate:                     associations,
			usercreate:                    usercreate,
			postAuthenticate:              postAuthenticate,
			authenticateToken:             authenticateToken,
			getUserByUsernameOrProfile:    getUserByUsernameOrProfile,
			createUserPartnerRelationship: createRelationshipWithPartners,
			generateHashedPassword: function(unhashedPW, cb){
				bcrypt.hash(unhashedPW, bcrypt.genSaltSync(8), cb);
			}
		},
		instanceMethods: {
			comparePassword: comparePassword,
			removeSensitiveValues: function(){
				delete this.dataValues.password;
				delete this.dataValues.verify_token;
				return JSON.stringify(this.dataValues);
			}
		},
		// validate:{
		// 	verification: function(){
		// 		if(this.verified === false && this.verify_token === null)
		// 			throw new Error('verify_token cannot be null if user verified is false')
		// 	},
		// 	passwordNullable: function() {
		// 		if( (this.password === null || this.password === '') === (this.verified === true) )
		// 			throw new Error('password can not be empty or null if user is verified')
		// 	}
		// }
	})

	User.hook('beforeCreate', beforeCreateUser)
	User.hook('beforeUpdate', beforeUpdateUser)

	return User;
};


function schema(datatypes){
	return {
		email: {
			type: datatypes.STRING(50),
			unique: true,
			allowNull: false,
			validate:{
				notEmpty: true
			}
		},

		password:{
			type: datatypes.STRING,
			allowNull: true,
			scope: ['self'],
			validate:{
				notEmpty: true
			}
		},

		verified: {
			type: datatypes.DATE,
			allowNull: true,
		},

		verify_token: {
			type: datatypes.STRING,
  			allowNull: true
		},

		token_expiry:{
			type: datatypes.DATE,
			allowNull: true
		}
	}
}

function associations(models) {
	User.hasOne(models.UserProfile)

	User.hasMany(models.PrivateMessage, { foreignKey: 'sender_id' })
	User.hasMany(models.PrivateNotification)
	User.hasMany(models.Post)
	User.hasMany(models.Media)

	User.belongsToMany(models.Relationship, { through: models.UserRelationship, foreignKey: 'user_id' })
	User.belongsToMany(models.User, { as: 'Partner', through: models.UserRelationship, foreignKey: 'user_id' })
}

function getUserByUsernameOrProfile(nameOrEmail, getBy, includePartner){
	includePartner = includePartner || true

	var includeModel = [{ model: sequelize.model('UserProfile') }]

	if(includePartner)
		includeModel.push( { model: sequelize.model('User'), as: 'Partner' } )


	if (getBy === 'user'){
		return sequelize.model('User').find({
			where: {email: nameOrEmail},
			include: includeModel
		})
	}
	else if (getBy === 'profile') {

		return sequelize.transaction(function ( t ){
			return sequelize.model('UserProfile').find({
				where: {username: nameOrEmail}
			},{
				transaction: t
			})
			.then(function(profile){
				return sequelize.model('User').find({
					where: {id: profile.getDataValue('user_id')},
					include: includeModel
				}, {
					transaction: t
				})
			})
		})
	}
}

function comparePassword(candidatePassword, cb) {
	/*jshint validthis:true */
	var savedHash = this.getDataValue('password').replace(/\\/g, '');

	bcrypt.compare(candidatePassword, savedHash, function(err, isMatch) {
		if(err) return cb(err);
		cb(null, isMatch);
	});
}

function beforeCreateUser(user, options, cb){
	if(user.password === null)
		return cb(null, user)

	User.generateHashedPassword(user.password, function(err, hashedPassword){
		if(err) return cb(err);
		user.password = hashedPassword;
		cb(null, user);
	});
}

function beforeUpdateUser(user, options, cb){
	//user.changed()      // will return an array of the attributes changed.
	//user.changed(key)   // will return an boolean for whether or not a specific attribute has changed.
	if(user.changed('password')){
		User.generateHashedPassword(user.password, function(err, hashedPassword){
			if(err) return cb(err);
			user.password = hashedPassword;
			cb(null, user);
		});
	}
}

function createRelationshipWithPartners (userDAO, partnerDAO){
	return $q.all([
		userDAO.getRelationships(),
		partnerDAO.getRelationships()
	])
	.spread(function(userRelationships, partnerRelationships){
		if(userRelationships.length > 0){
			return $q.fcall(function () {
				return {code: 202, reason: 'user already has a relationship'}
			})
		}

		if(partnerRelationships.length > 0){
			return $q.fcall(function () {
				return {code: 202, reason: 'partner already has a relationship'}
			})
		}

		return sequelize.model('Relationship').create({}).then(function(newRelationship){
			return sequelize.transaction().then(function (t) {
				return $q.all([
					// join user to relationship and auto confirm
					userDAO.addRelationship(newRelationship, {
						partner_id: partnerDAO.getDataValue('id'),
						confirmed: true
					}, { transaction: t }),

					// join partner to relationship but set confirm to false
					// as the partner will manually confirm
					partnerDAO.addRelationship(newRelationship, {
						partner_id: userDAO.getDataValue('id'),
						confirmed: false
					}, { transaction: t })
				])
				.then(function(usr, prtnr) {
					t.commit()
					return $q.fcall(function () {
						return {code: 201, relationship: newRelationship}
					})
				})
				.catch(function (err) {
					newRelationship.destroy({force: true})
					t.rollback()
					return $q.fcall(function(){
						throw new Error(err)
					})
				})
			})
		})
	})
}

function authenticateToken(creds, cb){
	if(!creds.user_id || !creds.token) return cb(new Error('provide both token and user id'))

	if(creds.user_id == creds.token) return cb(new Error('user_id and token should be different values'))

	var decodedUser = jwt.verify(creds.token, config.secret);

	if(decodedUser.id === creds.user_id)
		return cb(null, creds)
	else
		return cb(new Error('user_id and token user do not not match'))
}

function postAuthenticate(socket, userCreds){
	var deferred = $q.defer()

	sequelize.model('User').find({
		where: {id: userCreds.user_id},
		include: [sequelize.model('Relationship')]
	})
	.then(function(socketuser){
		socket.handshake.session.user = socketuser
		if(!socket.handshake.session.rooms) socket.handshake.session.rooms = {}
		socket.to(socket.id).emit('ready')
		deferred.resolve(userCreds)
	})
	.catch(function(err){
		deferred.reject(err)
	})

	return deferred.promise
}

function usercreate(email, password, verified){
	var today      = new Date()
	    verified   = (verified ? today : undefined)
	    password   = (verified ? password : null )
	var randomHash = (verified ? null : chance.string())
	var hashExpiry = (verified ? null : new Date(new Date().setDate(today.getDate() + 2)) ) // expires in 2 days time

	return User.create({
		email: email,
		password: password,
		verified: verified,
		verify_token: randomHash,
		token_expiry: hashExpiry
	})
}
