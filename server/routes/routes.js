var router           = require('express').Router()
var routeControllers = require(__dirname + '/../controllers')
var env              = process.env.NODE_ENV || "development";
var config           = require(__dirname + '/../config/database_config.json')[env];
var expressJwt       = require('express-jwt')



// middleware specific to this router
router.use(function timeLog(req, res, next) {
	console.log('Time: ', Date.now());
	next();
});

// router.use(busboy())


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/login', routeControllers.User.login);

router.route('/verifyuser/:user_id')
	.post(routeControllers.User.verifyUser)



router.route('/users')
	.get(routeControllers.User.getAll)
	.post(routeControllers.User.addUser)

router.route('/users/:user_id')
	.get( expressJwt({secret: config.secret, credentialsRequired: false}),
			routeControllers.User.getUser)
	.put( expressJwt({secret: config.secret, credentialsRequired: false}),
			routeControllers.User.updateUser)

router.route('/users/:user_id/avatar')
	// .get(routeControllers.User.getAvatar)
	.post(expressJwt({secret: config.secret, credentialsRequired: false}),
			routeControllers.User.updateAvatar)


router.route('/couples')
	.get(routeControllers.User.getCouples)
	.post(routeControllers.User.addUser)

router.route('/couples/:couple_id')
	.get(routeControllers.User.getCouple)

router.route('/couples/:couple_id/:user_id')
	.post(routeControllers.User.confirmRelationship)



router.get('/conversations', routeControllers.PrivateConversation.getAll)

// router.route('/conversations/id/:conversation_id')
	// .post(routeControllers.PrivateConversation.getAll)

router.route('/private-conversations/relationship/:relationship_id')
	.get(routeControllers.PrivateConversation.getbyRelationshipId)
	.post(routeControllers.PrivateConversation.create)
router.route('/private-conversations/:id')
	.get(routeControllers.PrivateConversation.getById)



router.route('/private-posts/:relationship_id')
	.get(expressJwt({secret: config.secret}), routeControllers.PrivatePost.getAllPrivatePosts)
	.post(expressJwt({secret: config.secret}), routeControllers.PrivatePost.addPrivatePost)

router.route('/private-posts/:relationship_id/:post_id')
	.get(expressJwt({secret: config.secret}), routeControllers.PrivatePost.getPrivatePost)
	.put(expressJwt({secret: config.secret}), routeControllers.PrivatePost.updatePrivatePost)




module.exports = router;
