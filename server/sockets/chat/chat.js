'use strict'

// var socketAuth = require('socketio-auth')
var _          = require('lodash')
var db         = require(__dirname + '/../../models')

var userRooms    = {}
var privateRooms = {}
var connectedSockets = {
	// "user_id": "socket_id",
}
var connectedRelationshipSockets = {
	// relationship_id:{
	//	user_id: socket_id,
	// 	user_id: socket_id
	// }
}


module.exports = function(socket, nsp){
	console.log('connected TO /CHAT namespace --------| \n\n')

	var socketUser      = socket.decoded_token
	var subscribedRooms = userRooms[socketUser.id]

	_.forEach(subscribedRooms, socket.join)

	socket.on('subscribe', function(user){
		var userId         = user.user_id
		// var relationshipId = user.relationship_id

		// db call: get all public conversations with user's relationshipid. then join room for each row returned by row id

		// if( !connectedRelationshipSockets[relationshipId] )
			// connectedRelationshipSockets[relationshipId] = {}

		// connectedRelationshipSockets[relationshipId][userId] = socket.id
		connectedSockets[userId] = socket.id
	})

	socket.on('privateMessage', function(messageData, cb){
		var receipientSocketId = connectedSockets[messageData.receipient],
		newMessage = {
    		sender_id: messageData.sender_id,
    		receipient_id: messageData.receipient_id,
    		message: messageData.message
    	}

    	db.sequelize
			.transaction(function(t){
		    	return db.PrivateConversation
					.findOrCreate({
			    			where: { relationship_id: messageData.relationship_id }
			    		}, { transaction: t})
					.spread(function(privateConvo, newCreation){
						newMessage.conversation_id = privateConvo.getDataValue('id')
						return db.PrivateMessage.create(newMessage, {transaction: t})
					})
	    	})
			.then(function(newMsg){
				if(cb) cb(null, newMsg)

				if(receipientSocketId && nsp.connected[receipientSocketId]){
					nsp
						.to(receipientSocketId)
						.emit('recievePrivateMessage', newMsg)
				}
			})

	})

	// socket.on('privateConversation:Subscribe', function(data){
	// 	var requestPartnerId   = data.userid
	// 	var requestUserPartner = _.filter(socketUser.Partner, { id: requestPartnerId })

	// 	if(requestUserPartner.length){
	// 		var relationshipId = requestUserPartner.UserRelationship.relationship_id

	// 		//check if not already subscribed to room, and subscribe to room if not
	// 		if( privateRooms[relationshipId] && privateRooms[relationshipId].indexOf(socketUser.id) < 0 ){
	// 			privateRooms[relationshipId] = [requestPartnerId, socketUser.id]

	// 			if(!userRooms[socketUser.id])
	// 				userRooms[socketUser.id] = [];

	// 			if(!userRooms[requestPartnerId])
	// 				userRooms[requestPartnerId] =[]

	// 			userRooms[socketUser.id].push(relationshipId)
	// 			userRooms[requestPartnerId].push(relationshipId);
	// 		}

	// 		socket.join(relationshipId)
	// 		socket.emit('privateConversation:Subscribed')
	// 	}
	// })

  	// socket.on('join room', function (roomid, done){
  	// 	var socketUser = socket.decoded_token
  	// 	var partnerWithSameRoomNumber = _.filter(socketUser.Partner, { UserRelationship: { relationship_id: roomid } } )

  	// 	if( partnerWithSameRoomNumber.length ){
  	// 		socket.join(roomid)

  	// 		db.PrivateConversation.find({ where: {relationship_id: roomid} }).then(function(privateConversationDAO){
  	// 			//Save DAO onto socket so that prvateMessage asscoiations can easily be created by getters and setters
  	// 			socket.handshake.session.rooms[roomid] = privateConversationDAO
  	// 			//send success feedback
  	// 			socket.to(socket.id).emit('room joined', {status: 200, room: roomid})
  	// 			done()
  	// 		})
  	// 	}else{
  	// 		socket.to(socket.id).emit('room failed', {
  	// 			status:400,
  	// 			room: roomid,
  	// 			message:'user does not belong to this relationship'
  	// 		})
  	// 		done('user does not belong to this reationship')
  	// 	}
  	// })

    // socket.on('newMessage', function (messageObj, cb) {
    // 	if(!messageObj.roomId){
    // 		cb('roomid not present in message object')
    // 		return socket.to(socket.id).emit('messageNotSent', { reason: 'roomid not present in message object'})
    // 	}

    // 	if(_.indexOf(userRooms[socketUser.id], messageObj.roomId) < 0 )
    // 		return cb('user cannot send messages to room if not joined to that room')

    // 	var isPrivate = ( privateRooms[messageObj.roomid] ? true : false )
    // 	var newMessage;

    // 	if(isPrivate){
	   //  	newMessage = db.PrivateMessage.build({
	   //  		conversation_id: messageObj.roomId,
	   //  		sender_id: socketUser.id,
	   //  		message: messageObj.message
	   //  	})
    // 	}

    // 	db.sequelize.transaction(function(t){
	   //  	return db.PrivateConversation.find({ where: {
	   //  		relationship_id: messageObj.roomId
	   //  	}},
	   //  	{
	   //  		transaction: t
	   //  	})
    // 		.then(function(privateConvo){
    // 			return privateConvo.addPrivateMessage(newMessage, {
    // 				save: false,
    // 				transaction: t
    // 			})

    // 		})
    // 	})
    // 	.then(function(result){
    // 		cb()
    // 		socket.to(messageObj.room_id).emit('receiveNewMessage', {
    // 			user_id: socketUser.id,
    // 			message: messageObj.message
    // 		})
    // 	})
    // 	.catch(function(err){
    // 		cb(err)
    // 		console.log(err)
    // 	})
    // });

    socket.on('disconnect', function(){
    	var disconnectedUserId = socket.decoded_token.user_id
    	delete connectedSockets[disconnectedUserId]
    });


}
