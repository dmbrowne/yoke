'use strict'

var env             = process.env.NODE_ENV || 'development';
var config          = require(__dirname + '/../config/database_config.json')[env];
var socketIOjwt     = require('socketio-jwt')
var ioChat          = require('./chat/chat')


module.exports = function(io){

	// io.use(socketIOjwt.authorize({
	// 	secret: config.secret,
	// 	handshake: true
	// }));

	// io.of('/chat').on('connection', function(socket){
	// 	console.log('\n\n\n Connect to chat namespace \n\n\n')
	// 	var ioChat = require('./chat/chat')(chatSocketNsp)
	// })

	// io.of('/').on('connection', function(socket){
	// 	console.log('\n\n\n Connect to root namespace \n\n\n')
	// })

	io.of('/').on('connection', socketIOjwt.authorize({
		secret: config.secret,
		timeout: 15000
	}))

	io
		.of('/chat')
		.on('connection', socketIOjwt.authorize({
			secret: config.secret,
			timeout: 15000
		}))
		.on('authenticated', function (socket) {
			console.log('\n\n Socket Connected \n\n\n\n')
			ioChat(socket, io.of('/chat'))
		})

	// io.sockets.on('authenticated', function (socket) {
	// 	console.log('\n\n Socket Connected \n\n\n\n')
	//
	// 	var nsp = socket.nsp.name
	// 	if(nsp === '/chat') ioChat(socket, io.of('/chat'))
	// });
}
