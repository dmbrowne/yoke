process.env.NODE_ENV = 'test'

var expect    = require('chai').expect;
var app        = require(__dirname + '/../../app')
var cloudinary = require('cloudinary')
var path = require('path')
var fs = require('fs')

describe('cloudinary synchronous test', function () {
	it('should run synchronously', function(done){
		this.timeout(10000)

		cloudinary.uploader.upload(
			path.normalize(__dirname + '/../uploads/daka.jpg'),
			function(result){
				console.log('this should run first')
				console.log(result)
				done()
			}
		)

		console.log('this should run after')
	})
})
