process.env.NODE_ENV = 'test'

var expect   = require('chai').expect;
var assert   = require('chai').assert;
var db       = require(__dirname + '/../../models')
var env      = process.env.NODE_ENV || "development";
var config   = require(__dirname + '/../../config/database_config.json')[env];
var jwt      = require('jsonwebtoken');

var testuser = {
	email: 'test@test.com',
	password: 'password'
}

var fakeuser = {
	verified: false,
	id: '555f60668a192512aac7de04',
	email: 'faux@faux.com',
	password: '$2a$08$aDOk8NvhCDdbbb.wsiWUWOhNejyiOty5rcoEFARkAkiMKfhgpx/A6',
	updated_at: '2015-05-22T16:59:18.179Z',
	created_at: '2015-05-22T16:59:18.179Z',
	deleted_at: null
}

describe('User Model Method', function(){
	describe('authenticate token', function(){

		before('create user and save token string', function(done){
			db.User.create(testuser).then(function(newUser){
				testuser.userDAO = newUser;
				testuser.token = jwt.sign(newUser, config.secret, { expiresInMinutes: 60*5 });
				if(testuser.token)
					done()
				else
					done('token didnt sign')
			})
			.catch(function(err){ done(err) })
		})

		after('remove test user', function(done){
			testuser.userDAO.destroy({force:true}).then(function(){
				done()
			}).catch(function(err){
				done(err)
			})
		})

		it('should return true if the supplied user_id and supplied token match', function(done){
			var isAuthenticated = db.User.authenticateToken({
				user_id: testuser.userDAO.getDataValue('id'),
				token: testuser.token
			})

			expect(isAuthenticated).to.be.true
			done()
		})

		it('should return false if the supplied user_id and supplied token do not match', function(done){
			var isAuthenticated = db.User.authenticateToken({
				user_id: testuser.userDAO.getDataValue('id'),
				token: jwt.sign(fakeuser, config.secret, { expiresInMinutes: 60*5 })
			})

			expect(isAuthenticated).to.not.be.true
			done()
		})
	})
})
