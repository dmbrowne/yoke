process.env.NODE_ENV = 'test'

var expect    = require('chai').expect;
var assert    = require('chai').assert;
var db        = require(__dirname + '/../../models')
var app        = require(__dirname + '/../../../app')
var supertest  = require('supertest')
var appRequest = supertest(app)


var relationship;

describe('Private Conversation', function(){

	describe('Create or Find a conversation', function(){

		before('create blank relationship', function(done){
			db.Relationship.create({}).then(function(relation){
				relationship = relation
				done()
			})
		})

		after('remove test relationship', function(done){
			relationship.getPrivateConversation().then(function(convos){
				convos.destroy({force: true}).then(function(){
					relationship.destroy({force: true}).then(function(){
						done()
					})
				})
			})
			.catch(function(err){ console.log(err); done(err) })
		})

		it('should return the conversation with relationship and messages', function(done){
			appRequest
				.get( '/api/v1/conversations/relationship/' + relationship.getDataValue('id') )
				.end(function(err, res){
					expect(res.body.relationship_id).to.equal(relationship.getDataValue('id'))
					// expect(res.body.Relationship.id).to.equal(relationship.getDataValue('id'))
					// assert.isArray(res.body.PrivateMessages)
					done()
				})
		})

	})

})
