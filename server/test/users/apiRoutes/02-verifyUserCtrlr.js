process.env.NODE_ENV = 'test'

var expect     = require('chai').expect
var supertest  = require('supertest')
var app        = require(__dirname + '/../../../../app')
var appRequest = supertest(app)
var db         = require(__dirname + '/../../../models')

describe('Verify User', function() {
	describe('#GET /verifyuser', function() {
		var user;

		before(function(done){
			db.User
				.findOrCreate({
					where: { email: 'user@test.com' },
					defaults: { email: 'user@test.com', password: 'password' }
				})
				.spread(function(createduser, created){
					user = createduser
					done()
				})
				.catch(done)
		})

		after(function(){
			return user.destroy({ force: true })
		})

		it('should be confirmed after visiting URL', function(done){
			appRequest
				.get('/api/v1/verifyuser')
				.query({
					id: user.getDataValue('password'),
					user_id: user.getDataValue('id')
				})
				.end(function(err, res){
					if(err) done(err)

					expect(res.status).to.be.at.least(200)
					expect(res.status).to.be.below(300)

					expect(res.body.email).to.equal( user.getDataValue('email') )
					expect(res.body.verified).to.be.true

					done()
				})
		})
	})
})

