process.env.NODE_ENV = 'test'

var db         = require(__dirname + '/../../../models')
var app        = require(__dirname + '/../../../../app')
var supertest  = require('supertest')
var appRequest = supertest(app)
var expect     = require('chai').expect
var _          = require('lodash')
var $q         = require('q')

var newAccount = {
	email: 'user@test.com',
	password: 'password',
	partner: {
		email: 'partner@test.com'
	}
}

describe('Create joint account', function () {
	describe('#POST /users', function () {

		before('create Joint Account', createJointAccountRouteCall)
		after('delete Test Users And Relationships', deleteTestUsersAndRelationships)

		it('should create a verifed user', function(done) {
			db.User
				.find({ where: { email: newAccount.email } })
				.then(function(user){
					// check new account has been created
					expect(user.getDataValue('email')).to.equal( newAccount.email )
					// user should be auto verified
					expect(user.getDataValue('verified')).to.be.true
					done()
				})
		})

		it('should create an unverifed partner', function(done) {
			db.User
				.find({ where: { email: newAccount.partner.email } })
				.then(function(user){
					// check new account has been created
					expect(user.getDataValue('email')).to.equal( newAccount.partner.email )
					// user should be auto verified
					expect(user.verifed).to.not.be.true
					done()
				})
		})
	})
})

function createJointAccountRouteCall(done){
	appRequest
		.post('/api/v1/users')
		.send(newAccount)
		.end(function(err, res){
			console.log(res.body)
			done()
		})
}

function deleteTestUsersAndRelationships(done){
	return db.sequelize.transaction().then(function(t){
		return db.User.findAll({
			where: { email: [ newAccount.email, newAccount.partner.email ] }
		},
		{
			transaction: t
		})
		.then(function(users){
			return db.sequelize.Promise.map(users, function(user){
				return user.getRelationships()
			})
			.then(function(rltnshpsArr){
				return db.sequelize.Promise.all(rltnshpsArr)
					.then(function(relationships){
						return db.sequelize.Promise.map(users, function(user){
							return user.setRelationships([])
						})
						.then(function(removingRltnshps){
							return db.sequelize.Promise.all(removingRltnshps)
								.then(function(){
									return db.sequelize.Promise.map(relationships, function(rltn){
										return rltn[0].getDataValue('id')
									})
									.then(function(duplicateRelationshipIds){
										var relationshipId = _.union(duplicateRelationshipIds)

										return db.sequelize.Promise.map(users, function(user){
											return db.UserProfile.destroy({
												where: { user_id: user.getDataValue('id') },
												force: true
											}, {
												transaction: t
											})
											.then(function(){
												return user.destroy({force: true}, { transaction: t })
											})
										})
										.then(function(){
											return db.Relationship.destroy({
												where: { id: relationshipId },
												force: true
											})
											.then(function(){
												t.commit()
												done()
											})
										})
									})
								})
						})
					})
				})
		})
		.catch(function(err){ t.rollback(); console.log(err); done(err) })
	})
}
