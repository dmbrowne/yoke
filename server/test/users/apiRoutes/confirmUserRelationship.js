process.env.NODE_ENV = 'test'

var db         = require(__dirname + '/../../../models')
var app        = require(__dirname + '/../../../../app')
var supertest  = require('supertest')
var appRequest = supertest(app)
var expect     = require('chai').expect
var $q         = require('q')
var _          = require('lodash')
var savedData  = {}
var testUsers  = {
	user:{
		email: 'user@test.com',
		password: 'testpassword'
	},
	partner: {
		email: 'partner@test.com',
		password: 'testpassword'
	}
}

describe('Confirm user relationship', function() {
	describe('#POST /couples/:couple_id/:user_id', function(){

		before('Create users, relationship and link them as unconfirmed', createUnconfirmedCouple)
		after('Delete users & relationship', deleteTestUserAndRelationship)

		it('should return 400 if confirm property is not in the request body', function(done){
			appRequest
				.post( '/api/v1/couples/'+ savedData.relationship.getDataValue('id') +'/'+ savedData.user.getDataValue('id') )
				.send({})
				.end(function(err, res){
					if(err) done(err)

					expect(res.status).to.equal(400)
					done()
				})
		})

		it('should should confirm user when confirmation property is set to true', function(done){
			db.UserRelationship.find({
				where: {
					relationship_id: savedData.relationship.getDataValue('id'),
					user_id: savedData.partner.getDataValue('id')
				}
			})
			.then(function(savedUserRelationAttrs){
				expect(savedUserRelationAttrs.getDataValue('confirmed')).to.not.be.true

				appRequest
					.post( '/api/v1/couples/'+ savedData.relationship.getDataValue('id') +'/'+ savedData.partner.getDataValue('id') )
					.send({
						confirm: true
					})
					.end(function(err, res){
						if(err) done(err)

						expect(res.status).to.equal(200)
						expect(res.body.confirmed).to.be.true
						done()
					})
			})
		})
	})
})

function createUnconfirmedCouple(){
	return db.sequelize.transaction(function(t){
		return $q.all([
			db.Relationship.create({}, { transaction: t }),
			db.User.create( testUsers.user, { transaction: t }),
			db.User.create( testUsers.partner, { transaction: t })
		])
		.spread(function(relationship, user, partner){
			savedData.user         = user
			savedData.partner      = partner
			savedData.relationship = relationship

			return db.UserRelationship.bulkCreate([
				{
					user_id:         user.getDataValue('id'),
					partner_id:      partner.getDataValue('id'),
					relationship_id: relationship.getDataValue('id'),
					confirmed:       true
				},
				{
					user_id:         partner.getDataValue('id'),
					partner_id:      user.getDataValue('id'),
					relationship_id: relationship.getDataValue('id'),
					confirmed:       false
				}
			], {
				transaction: t
			})
		})
	})
	.catch(function(err){
		console.log(err)
		throw new Error(err)
	})
}

function deleteTestUserAndRelationship(){
	return db.sequelize.transaction(function(t){
		return db.User.findAll({
			where: {
				email: {
					$or: [
						testUsers.user.email,
						testUsers.partner.email
					]
				}
			}
		}, {
			transaction: t
		})
		.then(function(users){
			var userIds = _.pluck(users, 'id')

			return db.UserRelationship.findAll({
				where: { user_id: userIds }
			}, { transaction: t })
			.then(function(userRelationship){
				var relationshipIds = _.pluck(userRelationship, 'relationship_id')

				return db.UserRelationship.destroy({
					where: { user_id: userIds },
					force: true
				}, { transaction: t })
				.then(function(){
					return  db.sequelize.Promise.all([
						db.Relationship.destroy({
							force: true,
							where: { id: relationshipIds }
						}, { transaction: t }),

						db.User.destroy({
							force: true,
							where: { id: userIds }
						}, { transaction: t })
					])
				})
			})
		})
	})
	.catch(function(err){
		console.log(err)
		throw new Error(err)
	})
}
