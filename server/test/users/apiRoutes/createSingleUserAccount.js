process.env.NODE_ENV = 'test'

var db         = require(__dirname + '/../../../models')
var app        = require(__dirname + '/../../../../app')
var supertest  = require('supertest')
var appRequest = supertest(app)
var expect     = require('chai').expect
var $q         = require('q')

describe('Create single account', function() {
	describe('#POST /users', function(){
		var newUser = {
			email: "testuser@test.com",
			password: "password"
		}

		after(function removeCreatedTestUsr(){
			return db.User.find({
				where: { email: newUser.email }
			})
			.then(function(user){
				return $q([
					db.UserProfile.destroy({ where: {user_id: user.getDataValue('id')} }),
					user.destroy({ force: true })
				])
			})
		})

		it("should return 400 error if email or password aren't present in the request body", function(done){
			appRequest
				.post('/api/v1/users')
				.send({})
				.end(function(err, res){
					expect(res.status).to.equal(400)
					done()
				})
		})

		it("should create and return the created user and be verified", function(done){
			appRequest
				.post('/api/v1/users')
				.send(newUser)
				.end(function(err, res){
					expect(res.body.user.email).to.equal(newUser.email)
					expect(res.body.user.verified).to.be.true
					done()
				})
		})
	})
})
