process.env.NODE_ENV = 'test'

var expect    = require('chai').expect;
var assert    = require('chai').assert;
var db        = require(__dirname + '/../../models')
var UserModel = db.User;
var $q        = require('q')

var testUsers  = {
	user:{
		email: 'user@test.com',
		password: 'testpassword'
	},
	partner: {
		email: 'partner@test.com',
		password: 'testpassword'
	}
}

describe('User Model', function(){

	describe('Create a relationship method', function(){

		describe('Creating a new relationship', function(){

			before('create 2 users for user and partner', createTwoUsers)
			after('remove test users and relationship', removeTestUsers)

			it('should create a relationship', function(done){
				db.User.createUserPartnerRelationship(
					testUsers.user.DAO,
					testUsers.partner.DAO
				)
				.then(function(res){
					expect(res.code).to.equal(201)
					expect(res.relationship.dataValues.id).to.not.be.undefined
					testUsers.relationship = res.relationship
					done()
				})
				.catch(function(err){
					done(err)
				})
			})

		})


		describe('Create a relationship when user/partner already has one', function(){

			before('create users and a relationship', createRelationship)
			after('remove test users and relationship', removeTestUsers)

			it('should throw an error', function(done){
				db.User.createUserPartnerRelationship(
					testUsers.user.DAO,
					testUsers.partner.DAO
				)
				.then(function(response){
					expect(response.code).to.equal(202)
					done()
				})
				.catch(function(err){
					done(err)
				})
			})

		})
	})
})

function createTwoUsers(done){
	$q.all([
		db.User.create(testUsers.user),
		db.User.create(testUsers.partner)
	])
	.spread(function(usr, prtnr){
		testUsers.user.DAO = usr
		testUsers.partner.DAO = prtnr
		done()
	})
	.catch(function(err){ done(err) })
}

function createRelationship(done){
	createTwoUsers(function(err){
		if(err) return done(err)

		db.Relationship.create({}).then(function(newRltnshp){
			testUsers.relationship = newRltnshp

			$q.all([
				testUsers.user.DAO.addRelationship(newRltnshp, {
					partner_id: testUsers.partner.DAO.getDataValue('id'),
					confirmed: true
				}),

				testUsers.partner.DAO.addRelationship(newRltnshp, {
					partner_id: testUsers.user.DAO.getDataValue('id'),
					confirmed: true
				})
			]).then(function(){ done() })
		})
	})
}

function removeTestUsers(done){
	return db.UserRelationship.destroy({
		where: { relationship_id: testUsers.relationship.dataValues.id },
		force: true
	})
	.then(function(){
		return $q.all([
			testUsers.relationship.destroy({ force: true }),
			testUsers.user.DAO.destroy({force: true}),
			testUsers.partner.DAO.destroy({force: true})
		]).then(function(){ done() })
	})
	.catch(function(err){
		console.log(err)
		done(err)
	})
}
