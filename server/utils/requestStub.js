var $q = require('q')

module.exports = function(requestObj, controllerFn){
	var deferred = $q.defer()
	var responseStub = {}

	responseStub.send = function(responseData){
		deferred.resolve(responseData)
	}

	responseStub.status = function(statusCode){
		this.json = function(jsonData){
			deferred.reject({ status: statusCode, error: jsonData })
		}
	}

	responseStub.sendStatus = function(statusCode){
		this.json = function(jsonData){
			deferred.reject({ status: statusCode, error: jsonData })
		}
	}

	controllerFn(requestObj, responseStub)

	return deferred.promise
}
